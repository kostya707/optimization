#pragma once
#include "alglib/ap.h"
#define N 52

extern double moment[N+1];
extern double wght[N];
extern alglib::real_1d_array gause_x;
extern alglib::real_1d_array gause_w;
extern alglib::ae_int_t n;
extern int n2;
extern alglib::ae_int_t info;
void coupled_UCT(double variables[], double energy[], double ps[]);
void coupled_phase_array(double parameter[], double energy[], double phases_array[]);