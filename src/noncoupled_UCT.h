#pragma once
#include "alglib/ap.h"
#define N2 15
#define N 52 // was 80

extern double moment_noncoupled[N+1];
extern double wght_noncoupled[N];
extern alglib::real_1d_array x;
extern alglib::real_1d_array w;
extern alglib::ae_int_t n_noncoupled;
extern int n2_noncoupled;
extern alglib::ae_int_t info_noncoupled;
void noncoupled_UCT(double variables[], double energy[], double ps[]);
void uncoupled_waves_array(double parameter[], double energy[], double phase_shift_array[]);

