// phases_uncoupled_uct.cpp: ���������� ����� ����� ��� ����������� ����������.
//

#include "alglib/stdafx.h"

#include "noncoupled_UCT.h"
#include <stdio.h>
#include <time.h>
#include <iostream>
#if (AE_COMPILER==AE_MSVC)
#pragma warning(disable:4100)
#pragma warning(disable:4127)
#pragma warning(disable:4702)
#pragma warning(disable:4996)
#endif
#include "alglib/alglibinternal.h"
#include "alglib/alglibmisc.h"
#include "alglib/linalg.h"
#include "alglib/statistics.h"
#include "alglib/dataanalysis.h"
#include "alglib/specialfunctions.h"
#include "alglib/solvers.h"
#include "alglib/optimization.h"
#include "alglib/diffequations.h"
#include "alglib/fasttransforms.h"
#include "alglib/integration.h"
#include "alglib/interpolation.h"
#include <fstream>
#include <cmath>
#include <iomanip>

using namespace std;

#define BC 1.0 // WAS 3.0, 0.1

#define hc 197.3269718
#define pi 3.1415926

#define mn 4.758187
#define mps_iv 0.699544
#define mps_is 2.776371
#define ms_iv 4.981580
#define mv_iv 3.929975
#define mv_is 3.966260





void points_noncoupled(void)
{
double FI = pi/4.;


alglib::gqgenerategausslegendre(N,info_noncoupled,x,w);

for(int i = 0; i < N; i++)
{ moment_noncoupled[i] = BC*tan(FI*(1. + x[i]));
  wght_noncoupled[i] = pi*w[i]*BC / (4.*cos(FI*(1. + x[i]))*cos(FI*(1. + x[i])));}
}
//-------------angular integral (n=2)--------------------------------------------------------------------------------------------------------------------------
double Qn2_noncoupled(double sqr_p1, double sqr_p2, double pp, double meson_mass, double cutoff, int J, int fl)
{
       //intermediate variables
double Q;
double sqr_mn = mn*mn;
double sqr_pp = pp*pp;
double E_p1 = sqrt(sqr_p1 + sqr_mn);
double E_p2 = sqrt(sqr_p2 + sqr_mn);
double x = (sqr_p1 + sqr_p2 + meson_mass*meson_mass - fl*(E_p1 - E_p2)*(E_p1 - E_p2))/(2.*pp);
double y = (sqr_p1 + sqr_p2 + cutoff*cutoff - fl*(E_p1 - E_p2)*(E_p1 - E_p2))/(2.*pp);
double sqr_y = y*y; 
double S = cutoff*cutoff - meson_mass*meson_mass;
double C = - 0.5*pi * S*S / (sqr_pp*pp);
double A = y - x;
double lx = 0.5*log((x+1.)/(x-1.));
double ly = 0.5*log((y+1.)/(y-1.));
double eps = 0.01;
       //evaluation Qn2_noncoupled
//small pp'
double C2 = -4*pi*S*S;
double L = (meson_mass*meson_mass + 2*E_p1*E_p2-2*mn*mn)*(cutoff*cutoff + 2*E_p1*E_p2-2*mn*mn)*(cutoff*cutoff + 2*E_p1*E_p2-2*mn*mn);
double T = C2/L;
double a,b;
a = meson_mass*meson_mass + 2*E_p1*E_p2-2*mn*mn;
b = cutoff*cutoff + 2*E_p1*E_p2-2*mn*mn;
if(pp<eps){
//if((sqrt(sqr_p1)<eps2) && (sqrt(sqr_p2)<eps2)){
switch(J)
{
case 0:
	{Q = T*(1 + 2.*pp*pp*(1./(a*a)+ 3./(b*b) + 2./(a*b)) );
	 break;}
case 1:
	{Q = T*((0.666666)*pp*(1./a + 2./b) + 1.6*pow(pp,3.0)*(4./(b*b*b)+1./(a*a*a) + 3./(a*b*b) +2./(a*a*b)));
	 break;}
case 2:
	{ Q = T*2.*pp*pp*(1./(a*a) +3./(b*b) + 2./(a*b))*(0.266666);
	 break;}
case 3:
	{Q = T*0.45714285*pp*pp*pp*(4./(b*b*b)+1./(a*a*a) + 3./(a*b*b) +2./(a*a*b)); 
	 break;}
default: 
	 Q = 0.0;
}
	return Q;
}
else{
if(J < 0) 
     Q = 0.;
     else
         if(J == 0)
				Q = (lx - ly)/(A*A) - 1./(A*(y*y-1.));
         else 
             if (J == 1)
	               Q = (x*lx - y*ly)/(A*A) + (ly - y/(y*y-1.))/A;
			 else
				 if(J == 2)
					  Q = (0.5*(3.*x*x-1.)*lx - 0.5*(3.*y*y-1.)*ly - 1.5*(x-y))/(A*A) + (3.*y*ly - (3.*sqr_y-2.)/(sqr_y-1.))/A;
				 else
					 if(J == 3)
						  Q = (0.5*x*(5.*x*x-3.)*lx - 0.5*y*(5.*sqr_y-3.)*ly - 2.5*(x*x-sqr_y))/(A*A)

						       + (1.5*(5.*sqr_y-1.)*ly - 0.5*y*(15.*sqr_y-13.)/(sqr_y-1.))/A; 

return C*Q;
}
}


double Qn4_noncoupled(double sqr_p1, double sqr_p2, double pp, double meson_mass, double cutoff, int J, int fl)
{
       //intermediate variables
double Q1,Q2,Q3,Q4,Q;
double sqr_mn = mn*mn;
double sqr_pp = pp*pp;
double E_p1 = sqrt(sqr_p1 + sqr_mn);
double E_p2 = sqrt(sqr_p2 + sqr_mn);
double ssqr_pp = sqr_pp*sqr_pp;
double x = (sqr_p1 + sqr_p2 + meson_mass*meson_mass - fl*(E_p1 - E_p2)*(E_p1 - E_p2))/(2.*pp);
double y = (sqr_p1 + sqr_p2 + cutoff*cutoff - fl*(E_p1 - E_p2)*(E_p1 - E_p2))/(2.*pp);
double sqr_y = y*y;
double S = cutoff*cutoff - meson_mass*meson_mass;
double sqr_S = S*S;
double C = - pi*sqr_S*sqr_S / (8.0*ssqr_pp*pp);
double A = y - x;
double sqr_A = A*A;
double ssqr_A = sqr_A*sqr_A;
double lx = log((x+1.)/(x-1.));
double ly = log((y+1.)/(y-1.));
double d = sqr_y - 1.; 
double sqr_d = d*d;
double eps = 0.01;
       //evaluation Qn4_noncoupled
//small pp'
double C2 = -4*pi*S*S*S*S;
double L = (meson_mass*meson_mass + 2*E_p1*E_p2-2*mn*mn)*(cutoff*cutoff + 2*E_p1*E_p2-2*mn*mn)*(cutoff*cutoff + 2*E_p1*E_p2-2*mn*mn)*(cutoff*cutoff + 2*E_p1*E_p2-2*mn*mn)*(cutoff*cutoff + 2*E_p1*E_p2-2*mn*mn);
double T = C2/L;
double a,b;
a = meson_mass*meson_mass + 2*E_p1*E_p2-2*mn*mn;
b = cutoff*cutoff + 2*E_p1*E_p2-2*mn*mn;
if(pp<eps){
//if((sqrt(sqr_p1)<eps2) && (sqrt(sqr_p2)<eps2)){
switch(J)
{
case 0:
	{Q = T*(1 + 2.*pp*pp*(1./(a*a) + 10./(b*b) + 4./(a*b)));
	 break;}
case 1:
	{Q = T*(0.666666*pp*(1./a + 4./b) + 1.6*pp*pp*pp*(20./(b*b*b) + 1./(a*a*a) + 10./(a*b*b) + 4./(a*a*b)) );
	 break;}
case 2:
	{ Q = T*2.*pp*pp*(1./(a*a) +10./(b*b) + 4./(a*b))*(0.266666);
	 break;}
case 3:
	{Q = T*0.45714285*pp*pp*pp*(20./(b*b*b)+1./(a*a*a) + 10./(a*b*b) +4./(a*a*b)); 
	 break;}
case 4:
	{Q = 0;
	break;}
default: 
	 Q = 0.0;
}
	return Q;
}
else{
switch(J)
{
case 0:
   {Q1 = 0.5*(lx - ly)/(sqr_A*sqr_A); 
	Q2 =  - 1./(sqr_A*A*d);
	Q3 = - y/(sqr_A*sqr_d);
	Q4 = - (3.*y*y+1.)/(3.*A*sqr_d*d);
	Q = Q1 + Q2 + Q3 + Q4;
	break;}
case 1:
   {Q1 = 0.5*(x*lx - y*ly)/(sqr_A*sqr_A); 
	Q2 = (0.5*ly - y/d)/(sqr_A*A);
	Q3 = - 1./(sqr_d*sqr_A);
	Q4 = - 4.*y/(3.*sqr_d*d*A);
	Q = Q1 + Q2 + Q3 + Q4;
	break;}
case 2:
   {Q1 = (0.25*(3.*x*x-1.)*lx - 0.25*(3.*y*y-1.)*ly - 1.5*(x-y))/(sqr_A*sqr_A); 
	Q2 =  (1.5*y*ly - (3.*y*y-2.)/d)/(sqr_A*A);
	Q3 = - (0.75*ly - (1.5*y*y -2.5)*y/sqr_d)/sqr_A;
	Q4 = - 4./(3.*sqr_d*d*A);
	Q = Q1 + Q2 + Q3 + Q4;
	break;}
case 3:
   {Q1 = (0.25*x*(5.*x*x-3.)*lx - 0.25*y*(5.*sqr_y-3.)*ly - 2.5*(x*x-sqr_y))/(sqr_A*sqr_A); 
	Q2 = (0.75*(5.*sqr_y-1.)*ly - 0.5*y*(15.*sqr_y-13.)/d)/(sqr_A*A);
	Q3 = - (3.75*y*ly - 0.5*(15.*sqr_y*sqr_y-25.*sqr_y+8.)/sqr_d)/sqr_A;
	Q4 = (7.5*ly - y*(15.*sqr_y*sqr_y-40.*sqr_y+33.)/(sqr_d*d))/(6.*A);
	Q = Q1 + Q2 + Q3 + Q4;
	break;}
case 4:
   {double ssqr_y = sqr_y*sqr_y; double sqr_x = x*x;
	Q1 = (((35.*sqr_x*sqr_x - 30.*sqr_x + 3)*lx - (35.*ssqr_y - 30.*sqr_y + 3)*ly)/16. - 35.*(sqr_x*x - sqr_y*y)/8. + 55.*(x - y)/24.)/(sqr_A*sqr_A); 
	Q2 = (1.25*(7.*sqr_y - 3.)*y*ly - (105.*ssqr_y -115.*sqr_y + 16.)/(6.*d))/(sqr_A*A);
	Q3 = -0.5*(1.25*(21.*sqr_y - 3.)*ly - y*(105.*ssqr_y - 190.*sqr_y + 81.)/(2*sqr_d))/sqr_A;
	Q4 = (52.5*y*ly - (105.*ssqr_y*sqr_y - 280.*ssqr_y + 231.*sqr_y - 48.)/(sqr_d*d))/(6.*A);
	Q = Q1 + Q2 + Q3 + Q4;
	break;}
default: 
	Q = 0.0;
}
return C*Q;
} //else end
}

//-------------potential (S=0, J)------------------------------------------------------------------------------------------------------------------------------
double VS0(double p1,double p2, double parameter[], int J, int T)
{
                  //intermediate variables
double coeff;
double ms_is,gs_is,cutoffs_is;
double sqr_p1 = p1*p1; 
double sqr_p2 = p2*p2;
double sqr_mn = mn*mn;
double E_p1 = sqrt(sqr_p1 + sqr_mn);
double E_p2 = sqrt(sqr_p2 + sqr_mn);
double EE = E_p1*E_p2;
double P = sqr_p1 + sqr_p2;
double pp = p1*p2;
double EE3 = 3.*EE + sqr_mn;
double piEE = pi*sqrt(EE);
double Vps,Vs,Vvv,Vvt,Vtt1,Vtt2,Vtotal;

                  //isospin dependence
if(T == 0) {ms_is = parameter[15]; gs_is = parameter[6]; cutoffs_is = parameter[7]; coeff = -3.0;} 
else {ms_is = parameter[16]; gs_is = parameter[8]; cutoffs_is = parameter[9]; coeff = 1.0;}

                  //ps contribution
double Qn2_noncoupled_1_ps_iv = (J*Qn2_noncoupled(sqr_p1,sqr_p2,pp,mps_iv,parameter[1],J-1,1) + (J+1)*Qn2_noncoupled(sqr_p1,sqr_p2,pp,mps_iv,parameter[1],J+1,1))/(2*J+1);

double Qn2_noncoupled_1_ps_is = (J*Qn2_noncoupled(sqr_p1,sqr_p2,pp,mps_is,parameter[3],J-1,1) + (J+1)*Qn2_noncoupled(sqr_p1,sqr_p2,pp,mps_is,parameter[3],J+1,1))/(2*J+1);

Vps = (EE - sqr_mn) * (coeff*parameter[0]*Qn2_noncoupled(sqr_p1,sqr_p2,pp,mps_iv,parameter[1],J,1) + parameter[2]*Qn2_noncoupled(sqr_p1,sqr_p2,pp,mps_is,parameter[3],J,1))

             - pp * (coeff*parameter[0]*Qn2_noncoupled_1_ps_iv + parameter[2]*Qn2_noncoupled_1_ps_is);

Vps = -Vps/(4.0*pi*pi*EE);

                  //s contribution
double Qn2_noncoupled_1_s_iv = (J*Qn2_noncoupled(sqr_p1,sqr_p2,pp,ms_iv,parameter[5],J-1,1) + (J+1)*Qn2_noncoupled(sqr_p1,sqr_p2,pp,ms_iv,parameter[5],J+1,1))/(2*J+1);

double Qn2_noncoupled_1_s_is = (J*Qn2_noncoupled(sqr_p1,sqr_p2,pp,ms_is,cutoffs_is,J-1,1) + (J+1)*Qn2_noncoupled(sqr_p1,sqr_p2,pp,ms_is,cutoffs_is,J+1,1))/(2*J+1);

Vs = (EE + sqr_mn) * (coeff*parameter[4]*Qn2_noncoupled(sqr_p1,sqr_p2,pp,ms_iv,parameter[5],J,1) + gs_is*Qn2_noncoupled(sqr_p1,sqr_p2,pp,ms_is,cutoffs_is,J,1))

             - pp * (coeff*parameter[4]*Qn2_noncoupled_1_s_iv + gs_is*Qn2_noncoupled_1_s_is);

Vs = Vs/(4.0*pi*pi*EE);

                  //v contribution
double (*Qn_tt)(double, double, double , double, double, int, int);
Qn_tt = Qn4_noncoupled;
            
double Qn4_noncoupled_0 = (*Qn_tt)(sqr_p1,sqr_p2,pp,mv_iv,parameter[13],J,1);
double Qn4_noncoupled_1 = (J*(*Qn_tt)(sqr_p1,sqr_p2,pp,mv_iv,parameter[13],J-1,1) + (J+1.)*(*Qn_tt)(sqr_p1,sqr_p2,pp,mv_iv,parameter[13],J+1,1))/(2.*J+1);

//vv
Vvv = (2.*EE - sqr_mn) * (parameter[10]*(*Qn_tt)(sqr_p1,sqr_p2,pp,mv_is,parameter[11],J,1) + coeff*parameter[12]*Qn4_noncoupled_0);

Vvv = -Vvv/(2.0*pi*pi*EE); 

//vt
Vvt = coeff*parameter[12]*parameter[14]*mn*(P*Qn4_noncoupled_0 - 2.*pp*Qn4_noncoupled_1);

Vvt = -Vvt/(4.0*pi*pi*EE*mn);

//tt
double Qn4_noncoupled_2 = ( J*(J-1.)*(*Qn_tt)(sqr_p1,sqr_p2,pp,mv_iv,parameter[13],J-2,1)/(2.*J-1) + (2.*J*J*(2.*J+3)-1.)*Qn4_noncoupled_0/((2.*J-1)*(2.*J+3))

				+(J+1.)*(J+2.)*(*Qn_tt)(sqr_p1,sqr_p2,pp,mv_iv,parameter[13],J+2,1)/(2.*J+3) )/(2.*J+1);

Vtt1 = coeff*parameter[12]*parameter[14]*parameter[14]*((pp*pp + P*(EE - 2.*sqr_mn) + 6.*sqr_mn*(EE - sqr_mn))*Qn4_noncoupled_0 - pp*(P + 4.*sqr_mn)*Qn4_noncoupled_1 - pp*pp*Qn4_noncoupled_2);
Vtt1 = - Vtt1 / (8.*pi*pi*EE*sqr_mn);

Vtt2 = coeff*parameter[12]*parameter[14]*parameter[14]*(E_p1 - E_p2)*(E_p1 - E_p2)*(-(EE - 5.*sqr_mn)*Qn4_noncoupled_0 + pp*Qn4_noncoupled_1);
Vtt2 = - Vtt2 / (8.*pi*pi*EE*sqr_mn);

Vtotal = Vps + Vs + Vvv + Vvt + Vtt1 + Vtt2;

return Vtotal; 
}
//-------------potential (S=1, J)------------------------------------------------------------------------------------------------------------------------------
double VS1(double p1,double p2, double parameter[], int J, int T)
{
                  //intermediate variables
double coeff;
double ms_is,gs_is,cutoffs_is;
double sqr_p1 = p1*p1; 
double sqr_p2 = p2*p2;
double sqr_mn = mn*mn;
double E_p1 = sqrt(sqr_p1 + sqr_mn);
double E_p2 = sqrt(sqr_p2 + sqr_mn);
double EE = E_p1*E_p2;
double P = sqr_p1 + sqr_p2;
double pp = p1*p2;
double EE3 = 3.*EE + sqr_mn;
double piEE = pi*sqrt(EE);
double Vps,Vs,Vvv,Vvt,Vtt1,Vtt2,Vtotal;

                  //isospin dependence
if(T == 0) {ms_is = parameter[15]; gs_is = parameter[6]; cutoffs_is = parameter[7]; coeff = -3.0;} 
else {ms_is = parameter[16]; gs_is = parameter[8]; cutoffs_is = parameter[9]; coeff = 1.0;}

                  //ps contribution
double Qn2_noncoupled_2_ps_iv = ((J+1)*Qn2_noncoupled(sqr_p1,sqr_p2,pp,mps_iv,parameter[1],J-1,1) + J*Qn2_noncoupled(sqr_p1,sqr_p2,pp,mps_iv,parameter[1],J+1,1))/(2*J+1);

double Qn2_noncoupled_2_ps_is = ((J+1)*Qn2_noncoupled(sqr_p1,sqr_p2,pp,mps_is,parameter[3],J-1,1) + J*Qn2_noncoupled(sqr_p1,sqr_p2,pp,mps_is,parameter[3],J+1,1))/(2*J+1);

Vps = -(EE - sqr_mn) * (coeff*parameter[0]*Qn2_noncoupled(sqr_p1,sqr_p2,pp,mps_iv,parameter[1],J,1) + parameter[2]*Qn2_noncoupled(sqr_p1,sqr_p2,pp,mps_is,parameter[3],J,1))

             + pp * (coeff*parameter[0]*Qn2_noncoupled_2_ps_iv + parameter[2]*Qn2_noncoupled_2_ps_is);

Vps = -Vps/(4.0*pi*pi*EE);

                  //s contribution
double Qn2_noncoupled_2_s_iv = ((J+1)*Qn2_noncoupled(sqr_p1,sqr_p2,pp,ms_iv,parameter[5],J-1,1) + J*Qn2_noncoupled(sqr_p1,sqr_p2,pp,ms_iv,parameter[5],J+1,1))/(2*J+1);

double Qn2_noncoupled_2_s_is = ((J+1)*Qn2_noncoupled(sqr_p1,sqr_p2,pp,ms_is,cutoffs_is,J-1,1) + J*Qn2_noncoupled(sqr_p1,sqr_p2,pp,ms_is,cutoffs_is,J+1,1))/(2*J+1);

Vs = (EE + sqr_mn) * (coeff*parameter[4]*Qn2_noncoupled(sqr_p1,sqr_p2,pp,ms_iv,parameter[5],J,1) + gs_is*Qn2_noncoupled(sqr_p1,sqr_p2,pp,ms_is,cutoffs_is,J,1))

             - pp * (coeff*parameter[4]*Qn2_noncoupled_2_s_iv + gs_is*Qn2_noncoupled_2_s_is);

Vs = Vs/(4.0*pi*pi*EE);

                  //v contribution
double Qn4_noncoupled_0 = Qn4_noncoupled(sqr_p1,sqr_p2,pp,mv_iv,parameter[13],J,1);

double Qn4_noncoupled_2s = ((J+1)*Qn4_noncoupled(sqr_p1,sqr_p2,pp,mv_is,parameter[13],J-1,1) + J*Qn4_noncoupled(sqr_p1,sqr_p2,pp,mv_is,parameter[13],J+1,1))/(2*J+1);

double Qn4_noncoupled_1 = (J*Qn4_noncoupled(sqr_p1,sqr_p2,pp,mv_iv,parameter[11],J-1,1) + (J+1)*Qn4_noncoupled(sqr_p1,sqr_p2,pp,mv_iv,parameter[11],J+1,1))/(2*J+1);

double Qn4_noncoupled_2 = ((J+1)*Qn4_noncoupled(sqr_p1,sqr_p2,pp,mv_iv,parameter[11],J-1,1) + J*Qn4_noncoupled(sqr_p1,sqr_p2,pp,mv_iv,parameter[11],J+1,1))/(2*J+1);

double Qn4_noncoupled_3 = ( (J*J-1)*Qn4_noncoupled(sqr_p1,sqr_p2,pp,mv_iv,parameter[13],J-2,1)/(2*J-1) + 2*J*(J+1)*(2*J+1)*Qn4_noncoupled_0/((2*J-1)*(2*J+3))

				+J*(J+2)*Qn4_noncoupled(sqr_p1,sqr_p2,pp,mv_iv,parameter[13],J+2,1)/(2*J+3) )/(2*J+1);

//vv
Vvv = EE*(parameter[10]*Qn4_noncoupled(sqr_p1,sqr_p2,pp,mv_is,parameter[11],J,1) + coeff*parameter[12]*Qn4_noncoupled_0)
   
      + pp*(parameter[10]*Qn4_noncoupled_2s + coeff*parameter[12]*Qn4_noncoupled_2);

Vvv = -Vvv/(2.0*pi*pi*EE); 

//vt
Vvt = coeff*parameter[12]*parameter[14]*mn*(-P*Qn4_noncoupled_0 + 2.*pp*Qn4_noncoupled_2);

Vvt = -Vvt/(4.0*pi*pi*EE*mn);

//tt
Vtt1 = coeff*parameter[12]*parameter[14]*parameter[14]*((pp*pp + P*(EE - 2.*sqr_mn) + 2.*sqr_mn*(EE - sqr_mn))*Qn4_noncoupled_0 + pp*(P + sqr_mn)*Qn4_noncoupled_1 
														
														- pp*(EE + sqr_mn + P)*Qn4_noncoupled_2 - pp*pp*Qn4_noncoupled_3);
Vtt1 = - Vtt1 / (8.*pi*pi*EE*sqr_mn);

Vtt2 = coeff*parameter[12]*parameter[14]*parameter[14]*(E_p1 - E_p2)*(E_p1 - E_p2)*((EE - sqr_mn)*Qn4_noncoupled_0 - pp*Qn4_noncoupled_2);
Vtt2 = Vtt2 / (8.*pi*pi*EE*sqr_mn);
 
                 //total potential
Vtotal = Vps + Vs + Vvv + Vvt + Vtt1 + Vtt2;

return Vtotal; 
}
//-------------potential (S=1, J=0)----------------------------------------------------------------------------------------------------------------------------
double V3P0(double p1,double p2, double parameter[], int J, int T)
{
                  //intermediate variables
double coeff;
double ms_is,gs_is,cutoffs_is;
double sqr_p1 = p1*p1; 
double sqr_p2 = p2*p2;
double sqr_mn = mn*mn;
double E_p1 = sqrt(sqr_p1 + sqr_mn);
double E_p2 = sqrt(sqr_p2 + sqr_mn);
double EE = E_p1*E_p2;
double P = sqr_p1 + sqr_p2;
double pp = p1*p2;
double EE3 = 3.*EE + sqr_mn;
double piEE = pi*sqrt(EE);
double Vps,Vs,Vvv,Vvt,Vtt1,Vtt2,Vtotal;

                  //isospin dependence
if(T == 0) {ms_is = parameter[15]; gs_is = parameter[6]; cutoffs_is = parameter[7]; coeff = -3.0;} 
else {ms_is = parameter[16]; gs_is = parameter[8]; cutoffs_is = parameter[9]; coeff = 1.0;}

                  //ps contribution
Vps = (EE - sqr_mn) * (coeff*parameter[0]*Qn2_noncoupled(sqr_p1,sqr_p2,pp,mps_iv,parameter[1],J+1,1) + parameter[2]*Qn2_noncoupled(sqr_p1,sqr_p2,pp,mps_is,parameter[3],J+1,1))

             - pp * (coeff*parameter[0]*Qn2_noncoupled(sqr_p1,sqr_p2,pp,mps_iv,parameter[1],J,1) + parameter[2]*Qn2_noncoupled(sqr_p1,sqr_p2,pp,mps_is,parameter[3],J,1));

Vps = -Vps/(4.0*pi*pi*EE);

                  //s contribution
Vs = (EE + sqr_mn) * (coeff*parameter[4]*Qn2_noncoupled(sqr_p1,sqr_p2,pp,ms_iv,parameter[5],J+1,1) + gs_is*Qn2_noncoupled(sqr_p1,sqr_p2,pp,ms_is,cutoffs_is,J+1,1))

             - pp * (coeff*parameter[4]*Qn2_noncoupled(sqr_p1,sqr_p2,pp,ms_iv,parameter[5],J,1) + gs_is*Qn2_noncoupled(sqr_p1,sqr_p2,pp,ms_is,cutoffs_is,J,1));

Vs = Vs/(4.0*pi*pi*EE);

                  //v contribution
double Qn4_noncoupled_0 = Qn4_noncoupled(sqr_p1,sqr_p2,pp,mv_iv,parameter[13],J,1);

double Qn4_noncoupled_1 = Qn4_noncoupled(sqr_p1,sqr_p2,pp,mv_iv,parameter[13],J+1,1);

//vv
Vvv = 2*pp*(parameter[10]*Qn4_noncoupled(sqr_p1,sqr_p2,pp,mv_is,parameter[11],J,1) + coeff*parameter[12]*Qn4_noncoupled_0)
   
      + sqr_mn*(parameter[10]*Qn4_noncoupled(sqr_p1,sqr_p2,pp,mv_is,parameter[11],J+1,1) + coeff*parameter[12]*Qn4_noncoupled_1);

Vvv = -Vvv/(2.0*pi*pi*EE); 

//vt
Vvt = coeff*parameter[12]*parameter[14]*mn*(6.*pp*Qn4_noncoupled_0 - 3.*P*Qn4_noncoupled_1);

Vvt = -Vvt/(4.0*pi*pi*EE*mn);

//tt
Vtt1 = coeff*parameter[12]*parameter[14]*parameter[14]*( - pp*(3.*P + 2.*EE - 10.*sqr_mn)*Qn4_noncoupled_0 / 3. 
														
		+ (P*(EE - 2.*sqr_mn) - 2.*sqr_mn*(EE - sqr_mn))*Qn4_noncoupled_1 + 2.*pp*(EE + sqr_mn)*Qn4_noncoupled(sqr_p1,sqr_p2,pp,mv_iv,parameter[13],J+2,1)/3.);

Vtt1 =  - Vtt1 / (8.0*pi*pi*EE*sqr_mn);

Vtt2 = coeff*parameter[12]*parameter[14]*parameter[14]*(E_p1 - E_p2)*(E_p1 - E_p2)*(pp*Qn4_noncoupled_0 - (EE + 3.*sqr_mn)*Qn4_noncoupled_1);

Vtt2 = - Vtt2 / (8.0*pi*pi*EE*sqr_mn); 

                 //total potential
Vtotal = Vps + Vs + Vvv + Vvt + Vtt1 + Vtt2;

return Vtotal;
}
//-----------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
void potential_matrix(double parameter[], double out_matrix[N][N],double (*V)(double, double, double [], int, int), int J, int T)
{
for(int i = 0; i < N; i++)
for(int j = i; j < N; j++)
     {out_matrix[i][j] = (*V)(moment_noncoupled[i],moment_noncoupled[j],parameter,J,T);
      out_matrix[j][i] = out_matrix[i][j];}
}
//--------------------uncv--------------------------------------------------------------------------------------------------------------------------------------
void ps_for_set(double parameter[], double energy[], double phase_shift[], int set,double (*V)(double, double, double [], int, int), int J, int T)
{          
	              //intermediate variables
double pot_mtrx[N][N];
double W[N+1];
//double A[N+1][N+1],A1[(N+1)*(N+1)],VST[N+1],RM[N+1];
alglib::real_2d_array A;
A.setlength(N+1,N+1);
alglib::real_1d_array VST, RM;
VST.setlength(N+1);
RM.setlength(N+1);
double EA,FI,SN,AK2,EF,FA,S1,P0;
int N1;
alglib::ae_int_t info;
alglib::densesolverreport rep;

potential_matrix(parameter,pot_mtrx,V,J,T);

N1 = N + 1;
FI = pi/4.;

for(int s = 0; s < set; s++)
{
EA = energy[s]/hc; 
P0 = sqrt(mn*EA/2.);
AK2 = P0*P0;
	   
moment_noncoupled[N] = P0;

SN = 0.0;

for(int i = 0; i < N; i++)
for(int j = i; j < N; j++)
   {A[i][j] = pot_mtrx[i][j];
    A[j][i] = A[i][j];}

for(int j = 0; j < N1; j++)
   {A[j][N] = (*V)(moment_noncoupled[j],moment_noncoupled[N],parameter,J,T);
    A[N][j] = A[j][N];}

for(int i = 0; i < N; i++)
    {W[i] = wght_noncoupled[i] / (moment_noncoupled[i]*moment_noncoupled[i] - AK2);
     SN = SN + W[i];}
W[N] = - SN;

for(int i = 0; i < N1; i++)
  {EF = moment_noncoupled[i]*moment_noncoupled[i]*W[i]*( sqrt(mn*mn + moment_noncoupled[i]*moment_noncoupled[i]) + sqrt(mn*mn + AK2) ) / 2.;
   for(int j = 0; j < N1; j++)     
      {S1 = EF*A[j][i];
      if (j == i)
         A[j][i] = 1.0 + S1;
	  else
		 A[j][i] = S1;}}
			 
for(int i = 0; i < N1; i++)
    VST[i] = (*V)(moment_noncoupled[i],moment_noncoupled[N],parameter,J,T);

/*for(int i = 0; i < N1; i++)
  for(int j = 0; j < N1; j++)
    A1[i*N1+j] = A[i][j];*/

//imsl_d_lin_sol_gen (N1, A1, VST, IMSL_RETURN_USER, RM, 0);
alglib::rmatrixsolve(A,N1,VST,info,rep,RM);
FA = atan(-pi*P0*sqrt(mn*mn + AK2)*RM[N]/2.);
phase_shift[s] = FA*180./pi;
}
}
//-------------------------------------------------------------------------------------------------------------------------------------------------------------
void uncoupled_waves(int J, int S, int T, double parameter[], double energy[], double phase_shift[], int set)
{
double (*V)(double, double, double [], int, int);
if(S==0) V = VS0;
else 
    if((S==1)&&(J==0)) V = V3P0;
	else V = VS1;
ps_for_set(parameter, energy, phase_shift, set, V,J,T);
}
void uncoupled_waves_array(double parameter[], double energy[], double phase_shift_array[])
{
	double ps_1S0[N2], ps_3P0[N2], ps_1P1[N2], ps_3P1[N2], ps_1D2[N2], ps_3D2[N2];
	points_noncoupled();
	uncoupled_waves(0, 0, 1, parameter, energy, ps_1S0, N2);
	uncoupled_waves(0, 1, 1, parameter, energy, ps_3P0, N2);
	uncoupled_waves(1, 0, 0, parameter, energy, ps_1P1, N2);
	uncoupled_waves(1, 1, 1, parameter, energy, ps_3P1, N2);
	uncoupled_waves(2, 0, 1, parameter, energy, ps_1D2, N2);
	uncoupled_waves(2, 1, 0, parameter, energy, ps_3D2, N2);
	
	for(int i = 0; i < N2; i ++)
	{
		phase_shift_array[i] = ps_1S0[i];
		phase_shift_array[N2 + i] = ps_1P1[i];
		phase_shift_array[2 * N2 + i] = ps_1D2[i];
		phase_shift_array[3 * N2 + i] = ps_3P0[i];
		phase_shift_array[4 * N2 + i] = ps_3P1[i];
		phase_shift_array[5 * N2 + i] = ps_3D2[i];
	}

}
//-----------------------meson parameters fit to Bonn----------------------------------------------------------------------------------------------------------------------
double UCT1_noncoupled[17] =
{14.5000,        //0
 2200.00/hc,     //1
 2.8534,         //2
 1200.00/hc,     //3
 1.6947,         //4
 2200.00/hc,     //5
 19.4434,        //6
 1538.13/hc,     //7
 10.8292,        //8
 2200.00/hc,     //9
 27.0000,        //10
 2035.59/hc,     //11
 1.3000,         //12
 1450.00/hc,     //13   
 5.8500,         //14
 717.7167/hc,    //15
 568.8612/hc};   //16
double UCT2_noncoupled[17] =
{13.395,    //0
 2500.00/hc,//1
 5.0,	    //2
 1219/hc,   //3
 5.0,	    //4
 2169/hc,	//5
 22.015,    //6
 1200/hc,	//7	
 5.514,	    //8
 2500/hc,	//9
 17.349,    //10
 2494/hc,   //11
 1.2,	    //12
 1593/hc,   //13
 6.1,	    //14
 691.78/hc,	//15
 510.62/hc};//16
double parameter_noncoupled[17] =
{14.4,        //0
 1700/hc,     //1
 3.0,         //2
 1500/hc,     //3
 2.488,       //4
 2000/hc,     //5
 18.3773,     //6
 2000/hc,     //7
 8.9437,      //8
 1900/hc,     //9
 24.5,        //10
 1850/hc,     //11
 0.9,         //12
 1850/hc,     //13
 6.1,         //14
 720/hc,      //15
 550/hc};     //16

double UCT_new_16_noncoupled[17] = 
{14.692262,        //0
 2090.000148/hc,     //1
 2.996045,         //2
 1259.838216/hc,     //3
 1.619675,         //4
 2309.999639/hc,     //5
 19.917764,        //6
 1479.191115/hc,     //7
 10.597956,        //8
 2308.755933/hc,     //9
 28.349280,        //10
 2131.021345/hc,     //11
 1.309068,         //12
 1426.139170/hc,     //13   
 5.666928,         //14
 724.919940/hc,    //15
 563.962093/hc};   //16

double UCT_new_6_noncoupled[17] = 
{14.645990,        //0
 2090.000031/hc,     //1
 2.996099,         //2
 1259.999597/hc,     //3
 1.779400,         //4
 2309.999922/hc,     //5
 18.488151,        //6
 1461.223500/hc,     //7
 11.090982,        //8
 2091.675414/hc,     //9
 28.35,        //10
 2137.369469/hc,     //11
 1.318188,         //12
 1423.098507/hc,     //13   
 5.5575,         //14
 705.271538/hc,    //15
 571.568089/hc};   //16

void noncoupled_UCT(double variables[], double energy[], double ps[]) 
{
//points_noncoupled();
uncoupled_waves_array(variables, energy, ps);


}

