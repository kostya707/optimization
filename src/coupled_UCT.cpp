// ps_coupled_uct
#include "alglib/stdafx.h"

#include <stdio.h>
#include <time.h>
#include <iostream>
#if (AE_COMPILER==AE_MSVC)
#pragma warning(disable:4100)
#pragma warning(disable:4127)
#pragma warning(disable:4702)
#pragma warning(disable:4996)
#endif
#include "alglib/alglibinternal.h"
#include "alglib/alglibmisc.h"
#include "alglib/linalg.h"
#include "alglib/statistics.h"
#include "alglib/dataanalysis.h"
#include "alglib/specialfunctions.h"
#include "alglib/solvers.h"
#include "alglib/optimization.h"
#include "alglib/diffequations.h"
#include "alglib/fasttransforms.h"
#include "alglib/integration.h"
#include "alglib/interpolation.h"
#include <fstream>
#include <cmath>
#include <iomanip>
#include "coupled_UCT.h"
#include "noncoupled_UCT.h"

using namespace std;

#define BC 1.0 // was 3.0

#define hc 197.3269718
#define pi 3.1415926

#define mn 4.758187
#define mps_iv 0.699544
#define mps_is 2.776371
#define ms_iv 4.981580
#define mv_iv 3.929975
#define mv_is 3.966260


//-------------------------------------------------------------------------------------------------------------------------------------------------------------
void points(void)
{
double FI = pi/4.;

alglib::gqgenerategausslegendre(N,info,gause_x,gause_w);
for(int i = 0; i < N; i++)
{ moment[i] = BC*tan(FI*(1. + gause_x[i]));
  wght[i] = pi*gause_w[i]*BC / (4.*cos(FI*(1. + gause_x[i]))*cos(FI*(1. + gause_x[i])));}
}

//-------------angular integral (n=2)--------------------------------------------------------------------------------------------------------------------------
double Qn2(double sqr_p1, double sqr_p2, double pp, double meson_mass, double cutoff, int J, int fl)
{
       //intermediate variables
double Q;
double sqr_mn = mn*mn;
double sqr_pp = pp*pp;
double E_p1 = sqrt(sqr_p1 + sqr_mn);
double E_p2 = sqrt(sqr_p2 + sqr_mn);
double x = (sqr_p1 + sqr_p2 + meson_mass*meson_mass - fl*(E_p1 - E_p2)*(E_p1 - E_p2))/(2.*pp);
double y = (sqr_p1 + sqr_p2 + cutoff*cutoff - fl*(E_p1 - E_p2)*(E_p1 - E_p2))/(2.*pp);
double sqr_y = y*y; 
double S = cutoff*cutoff - meson_mass*meson_mass;
double C = - 0.5*pi * S*S / (sqr_pp*pp);
double A = y - x;
double lx = 0.5*log((x+1.)/(x-1.));
double ly = 0.5*log((y+1.)/(y-1.));
double eps = 0.1;
       //evaluation Qn2
//small pp'
double C2 = -4*pi*S*S;
double L = (meson_mass*meson_mass + 2*E_p1*E_p2-2*mn*mn)*(cutoff*cutoff + 2*E_p1*E_p2-2*mn*mn)*(cutoff*cutoff + 2*E_p1*E_p2-2*mn*mn);
double T = C2/L;
double a,b;
a = meson_mass*meson_mass + 2*E_p1*E_p2-2*mn*mn;
b = cutoff*cutoff + 2*E_p1*E_p2-2*mn*mn;
if(pp<eps){
//if((sqrt(sqr_p1)<eps2) && (sqrt(sqr_p2)<eps2)){
switch(J)
{
case 0:
	{Q = T*(1 + 2.*pp*pp*(1./(a*a)+ 3./(b*b) + 2./(a*b)) );
	 break;}
case 1:
	{Q = T*((0.666666)*pp*(1./a + 2./b) + 1.6*pow(pp,3.0)*(4./(b*b*b)+1./(a*a*a) + 3./(a*b*b) +2./(a*a*b)));
	 break;}
case 2:
	{ Q = T*2.*pp*pp*(1./(a*a) +3./(b*b) + 2./(a*b))*(0.266666);
	 break;}
case 3:
	{Q = T*0.45714285*pp*pp*pp*(4./(b*b*b)+1./(a*a*a) + 3./(a*b*b) +2./(a*a*b)); 
	 break;}
default: 
	 Q = 0.0;
}
	return Q;
}
else{
if(J < 0) 
     Q = 0.;
     else
         if(J == 0)
				Q = (lx - ly)/(A*A) - 1./(A*(y*y-1.));
         else 
             if (J == 1)
	               Q = (x*lx - y*ly)/(A*A) + (ly - y/(y*y-1.))/A;
			 else
				 if(J == 2)
					  Q = (0.5*(3.*x*x-1.)*lx - 0.5*(3.*y*y-1.)*ly - 1.5*(x-y))/(A*A) + (3.*y*ly - (3.*sqr_y-2.)/(sqr_y-1.))/A;
				 else
					 if(J == 3)
						  Q = (0.5*x*(5.*x*x-3.)*lx - 0.5*y*(5.*sqr_y-3.)*ly - 2.5*(x*x-sqr_y))/(A*A)

						       + (1.5*(5.*sqr_y-1.)*ly - 0.5*y*(15.*sqr_y-13.)/(sqr_y-1.))/A; 

return C*Q;
}
}


//-------------angular integral (n=4)--------------------------------------------------------------------------------------------------------------------------
double Qn4(double sqr_p1, double sqr_p2, double pp, double meson_mass, double cutoff, int J, int fl)
{
       //intermediate variables
double Q1,Q2,Q3,Q4,Q;
double sqr_mn = mn*mn;
double sqr_pp = pp*pp;
double E_p1 = sqrt(sqr_p1 + sqr_mn);
double E_p2 = sqrt(sqr_p2 + sqr_mn);
double ssqr_pp = sqr_pp*sqr_pp;
double x = (sqr_p1 + sqr_p2 + meson_mass*meson_mass - fl*(E_p1 - E_p2)*(E_p1 - E_p2))/(2.*pp);
double y = (sqr_p1 + sqr_p2 + cutoff*cutoff - fl*(E_p1 - E_p2)*(E_p1 - E_p2))/(2.*pp);
double sqr_y = y*y;
double S = cutoff*cutoff - meson_mass*meson_mass;
double sqr_S = S*S;
double C = - pi*sqr_S*sqr_S / (8.0*ssqr_pp*pp);
double A = y - x;
double sqr_A = A*A;
double ssqr_A = sqr_A*sqr_A;
double lx = log((x+1.)/(x-1.));
double ly = log((y+1.)/(y-1.));
double d = sqr_y - 1.; 
double sqr_d = d*d;
double eps = 0.1;
       //evaluation Qn4
//small pp'
double C2 = -4*pi*S*S*S*S;
double L = (meson_mass*meson_mass + 2*E_p1*E_p2-2*mn*mn)*(cutoff*cutoff + 2*E_p1*E_p2-2*mn*mn)*(cutoff*cutoff + 2*E_p1*E_p2-2*mn*mn)*(cutoff*cutoff + 2*E_p1*E_p2-2*mn*mn)*(cutoff*cutoff + 2*E_p1*E_p2-2*mn*mn);
double T = C2/L;
double a,b;
a = meson_mass*meson_mass + 2*E_p1*E_p2-2*mn*mn;
b = cutoff*cutoff + 2*E_p1*E_p2-2*mn*mn;
if(pp<eps){
//if((sqrt(sqr_p1)<eps2) && (sqrt(sqr_p2)<eps2)){
switch(J)
{
case 0:
	{Q = T*(1 + 2.*pp*pp*(1./(a*a) + 10./(b*b) + 4./(a*b)));
	 break;}
case 1:
	{Q = T*(0.666666*pp*(1./a + 4./b) + 1.6*pp*pp*pp*(20./(b*b*b) + 1./(a*a*a) + 10./(a*b*b) + 4./(a*a*b)) );
	 break;}
case 2:
	{ Q = T*2.*pp*pp*(1./(a*a) +10./(b*b) + 4./(a*b))*(0.266666);
	 break;}
case 3:
	{Q = T*0.45714285*pp*pp*pp*(20./(b*b*b)+1./(a*a*a) + 10./(a*b*b) +4./(a*a*b)); 
	 break;}
case 4:
	{Q = 0;
	break;}
default: 
	 Q = 0.0;
}
	return Q;
}
else{
switch(J)
{
case 0:
   {Q1 = 0.5*(lx - ly)/(sqr_A*sqr_A); 
	Q2 =  - 1./(sqr_A*A*d);
	Q3 = - y/(sqr_A*sqr_d);
	Q4 = - (3.*y*y+1.)/(3.*A*sqr_d*d);
	Q = Q1 + Q2 + Q3 + Q4;
	break;}
case 1:
   {Q1 = 0.5*(x*lx - y*ly)/(sqr_A*sqr_A); 
	Q2 = (0.5*ly - y/d)/(sqr_A*A);
	Q3 = - 1./(sqr_d*sqr_A);
	Q4 = - 4.*y/(3.*sqr_d*d*A);
	Q = Q1 + Q2 + Q3 + Q4;
	break;}
case 2:
   {Q1 = (0.25*(3.*x*x-1.)*lx - 0.25*(3.*y*y-1.)*ly - 1.5*(x-y))/(sqr_A*sqr_A); 
	Q2 =  (1.5*y*ly - (3.*y*y-2.)/d)/(sqr_A*A);
	Q3 = - (0.75*ly - (1.5*y*y -2.5)*y/sqr_d)/sqr_A;
	Q4 = - 4./(3.*sqr_d*d*A);
	Q = Q1 + Q2 + Q3 + Q4;
	break;}
case 3:
   {Q1 = (0.25*x*(5.*x*x-3.)*lx - 0.25*y*(5.*sqr_y-3.)*ly - 2.5*(x*x-sqr_y))/(sqr_A*sqr_A); 
	Q2 = (0.75*(5.*sqr_y-1.)*ly - 0.5*y*(15.*sqr_y-13.)/d)/(sqr_A*A);
	Q3 = - (3.75*y*ly - 0.5*(15.*sqr_y*sqr_y-25.*sqr_y+8.)/sqr_d)/sqr_A;
	Q4 = (7.5*ly - y*(15.*sqr_y*sqr_y-40.*sqr_y+33.)/(sqr_d*d))/(6.*A);
	Q = Q1 + Q2 + Q3 + Q4;
	break;}
case 4:
   {double ssqr_y = sqr_y*sqr_y; double sqr_x = x*x;
	Q1 = (((35.*sqr_x*sqr_x - 30.*sqr_x + 3)*lx - (35.*ssqr_y - 30.*sqr_y + 3)*ly)/16. - 35.*(sqr_x*x - sqr_y*y)/8. + 55.*(x - y)/24.)/(sqr_A*sqr_A); 
	Q2 = (1.25*(7.*sqr_y - 3.)*y*ly - (105.*ssqr_y -115.*sqr_y + 16.)/(6.*d))/(sqr_A*A);
	Q3 = -0.5*(1.25*(21.*sqr_y - 3.)*ly - y*(105.*ssqr_y - 190.*sqr_y + 81.)/(2*sqr_d))/sqr_A;
	Q4 = (52.5*y*ly - (105.*ssqr_y*sqr_y - 280.*ssqr_y + 231.*sqr_y - 48.)/(sqr_d*d))/(6.*A);
	Q = Q1 + Q2 + Q3 + Q4;
	break;}
default: 
	Q = 0.0;
}
return C*Q;
} //else end
}

double UCT1[17] =
{14.5000,        //0
 2200.00/hc,     //1
 2.8534,         //2
 1200.00/hc,     //3
 1.6947,         //4
 2200.00/hc,     //5
 19.4434,        //6
 1538.13/hc,     //7
 10.8292,        //8
 2200.00/hc,     //9
 27.0000,        //10
 2035.59/hc,     //11
 1.3000,         //12
 1450.00/hc,     //13   
 5.8500,         //14
 717.7167/hc,    //15
 568.8612/hc};   //16
double UCT2[17] =
{13.395,    //0
 2500.00/hc,//1
 5.0,	    //2
 1219/hc,   //3
 5.0,	    //4
 2169/hc,	//5
 22.015,    //6
 1200/hc,	//7	
 5.514,	    //8
 2500/hc,	//9
 17.349,    //10
 2494/hc,   //11
 1.2,	    //12
 1593/hc,   //13
 6.1,	    //14
 691.78/hc,	//15
 510.62/hc};//16
double parameter[17] =
{14.4,        //0
 1700/hc,     //1
 3.0,         //2
 1500/hc,     //3
 2.488,       //4
 2000/hc,     //5
 18.3773,     //6
 2000/hc,     //7
 8.9437,      //8
 1900/hc,     //9
 24.5,        //10
 1850/hc,     //11
 0.9,         //12
 1850/hc,     //13
 6.1,         //14
 720/hc,      //15
 550/hc};     //16
static double UCT_new_16[17] = 
{14.692262,        //0
 2090.000148/hc,     //1
 2.996045,         //2
 1259.838216/hc,     //3
 1.619675,         //4
 2309.999639/hc,     //5
 19.917764,        //6
 1479.191115/hc,     //7
 10.597956,        //8
 2308.755933/hc,     //9
 28.349280,        //10
 2131.021345/hc,     //11
 1.309068,         //12
 1426.139170/hc,     //13   
 5.666928,         //14
 724.919940/hc,    //15
 563.962093/hc};   //16

double UCT_new_6[17] = 
{14.645990,        //0
 2090.000031/hc,     //1
 2.996099,         //2
 1259.999597/hc,     //3
 1.779400,         //4
 2309.999922/hc,     //5
 18.488151,        //6
 1461.223500/hc,     //7
 11.090982,        //8
 2091.675414/hc,     //9
 28.35,        //10
 2137.369469/hc,     //11
 1.318188,         //12
 1423.098507/hc,     //13   
 5.5575,         //14
 705.271538/hc,    //15
 571.568089/hc};   //16

double V1(double p1,double p2, double parameter[], int J, int T)
{
                  //intermediate variables
double coeff;
double ms_is,gs_is,cutoffs_is;
double sqr_p1 = p1*p1; 
double sqr_p2 = p2*p2;
double sqr_mn = mn*mn;
double E_p1 = sqrt(sqr_p1 + sqr_mn);
double E_p2 = sqrt(sqr_p2 + sqr_mn);
double EE = E_p1*E_p2;
double P = sqr_p1 + sqr_p2;
double pp = p1*p2;
double EE3 = 3.*EE + sqr_mn;
double sqrt_j = sqrt((double)J*(J+1));
double Vps,Vs,Vvv,Vvt,Vtt_cor,Vtt,Vtotal;

                  //isospin dependence
if(T == 0) {ms_is = parameter[15]; gs_is = parameter[6]; cutoffs_is = parameter[7]; coeff = -3.0;} 
else {ms_is = parameter[16]; gs_is = parameter[8]; cutoffs_is = parameter[9]; coeff = 1.0;}

                  //ps contribution
Vps = - (EE - sqr_mn) * (coeff*parameter[0]*Qn2(sqr_p1,sqr_p2,pp,mps_iv,parameter[1],J-1,1) + parameter[2]*Qn2(sqr_p1,sqr_p2,pp,mps_is,parameter[3],J-1,1))

             + pp * (coeff*parameter[0]*Qn2(sqr_p1,sqr_p2,pp,mps_iv,parameter[1],J,1) + parameter[2]*Qn2(sqr_p1,sqr_p2,pp,mps_is,parameter[3],J,1));

Vps = - Vps/(4.0*pi*pi*EE*(2*J+1));

                  //s contribution
int JJ = (2*J+1)*(2*J+1);

Vs = - (EE + sqr_mn) * (coeff*parameter[4]*Qn2(sqr_p1,sqr_p2,pp,ms_iv,parameter[5],J-1,1) + gs_is*Qn2(sqr_p1,sqr_p2,pp,ms_is,cutoffs_is,J-1,1))

     + pp * JJ * (coeff*parameter[4]*Qn2(sqr_p1,sqr_p2,pp,ms_iv,parameter[5],J,1) + gs_is*Qn2(sqr_p1,sqr_p2,pp,ms_is,cutoffs_is,J,1))

	 -2*J*(J+1) * ( (E_p1-mn)*(E_p2-mn)*(coeff*parameter[4]*Qn2(sqr_p1,sqr_p2,pp,ms_iv,parameter[5],J+1,1) + gs_is*Qn2(sqr_p1,sqr_p2,pp,ms_is,cutoffs_is,J+1,1))

	               + (E_p1+mn)*(E_p2+mn)*(coeff*parameter[4]*Qn2(sqr_p1,sqr_p2,pp,ms_iv,parameter[5],J-1,1) + gs_is*Qn2(sqr_p1,sqr_p2,pp,ms_is,cutoffs_is,J-1,1)) ); 

Vs = - Vs/(4.0*pi*pi*EE*JJ);

                  //v contribution
double Qn4_0 = Qn4(sqr_p1,sqr_p2,pp,mv_iv,parameter[13],J,1);
double Qn4_1iv = Qn4(sqr_p1,sqr_p2,pp,mv_iv,parameter[13],J-1,1);
double Qn4_2iv = Qn4(sqr_p1,sqr_p2,pp,mv_iv,parameter[13],J+1,1);
double Qn4_1is = Qn4(sqr_p1,sqr_p2,pp,mv_is,parameter[11],J-1,1);
double Qn4_2is = Qn4(sqr_p1,sqr_p2,pp,mv_is,parameter[11],J+1,1);
double I1_is = (J*Qn4_1is + (J+1)*Qn4_2is) / (2*J+1);
double I2_is = ((J+1)*Qn4_1is + J*Qn4_2is) / (2*J+1);
double I1_iv = (J*Qn4_1iv + (J+1)*Qn4_2iv) / (2*J+1);
double I2_iv = ((J+1)*Qn4_1iv + J*Qn4_2iv) / (2*J+1);
double I3_iv = sqrt_j*(Qn4_2iv - Qn4_1iv) / (2*J+1);
double Qn4_3iv = Qn4(sqr_p1,sqr_p2,pp,mv_iv,parameter[13],J-2,1);
double Qn4_4iv = Qn4(sqr_p1,sqr_p2,pp,mv_iv,parameter[13],J+2,1);
double I11 = ((J-1)*Qn4_3iv + J*Qn4_0)/(2*J-1);
double I4 = ( J*(J-1)*Qn4_3iv/(2*J-1) + (2*J*J*(2*J+3)-1)*Qn4_0/((2*J-1)*(2*J+3)) + (J+1)*(J+2)*Qn4_4iv/(2*J+3) ) / (2*J+1);
double I5 = ( (J*J-1)*Qn4_3iv/(2*J-1) + 2*J*(J+1)*(2*J+1)*Qn4_0/((2*J-1)*(2*J+3)) + J*(J+2)*Qn4_4iv/(2*J+3) ) / (2*J+1);
double I6 = - sqrt_j*( (J-1)*Qn4_3iv/(2*J-1) + (2*J+1)*Qn4_0/((2*J-1)*(2*J+3)) - (J+2)*Qn4_4iv/(2*J+3) ) / (2*J+1);

//vv
Vvv = (3*J+1) * pp * (parameter[10]*Qn4(sqr_p1,sqr_p2,pp,mv_is,parameter[11],J,1) + coeff*parameter[12]*Qn4_0)

     + J * sqr_mn * (parameter[10]*I1_is + coeff*parameter[12]*I1_iv) + (J+1) * EE * (parameter[10]*I2_is + coeff*parameter[12]*I2_iv)

	 - J*(J+1) * mn * (E_p1 + E_p2) * ( parameter[10]*(Qn4_2is - Qn4_1is) + coeff*parameter[12]*(Qn4_2iv - Qn4_1iv) ) / (2*J+1);

Vvv = - Vvv/(2.0*pi*pi*EE*(2*J+1)); 

//vt
double Vvt_12 = mn * (6.*pp*Qn4_0 - 3.*P*I1_iv);
double Vvt_34 = mn * (2.*pp*Qn4_0 - P*I2_iv);
double Vvt_55 = (E_p1*sqr_p2 + 3.*E_p2*sqr_p1) * I3_iv;
double Vvt_66 = (E_p2*sqr_p1 + 3.*E_p1*sqr_p2) * I3_iv;
Vvt = coeff*parameter[12]*parameter[14]*(J*Vvt_12 + (J+1)*Vvt_34 + sqrt_j*(Vvt_55 + Vvt_66)) / (2*J+1);

Vvt = - Vvt/(4.0*pi*pi*EE*mn);

//tt
double Vtt_12 = (4.*sqr_mn - 3.*P)*pp*Qn4_0 + (6.*pp*pp - P*(EE + 3.*sqr_mn))*I1_iv + 2.*(EE + sqr_mn)*pp*I4;
double Vtt_34 = -(P + 4.*EE)*pp*Qn4_0 - 2.*pp*pp*I1_iv + (4.*pp*pp + P*(EE - sqr_mn))*I2_iv + 2.*(EE + sqr_mn)*pp*I5;
double Vtt_55 = mn * ( (E_p1*P + E_p2*(3.*sqr_p1 - sqr_p2))*I3_iv - 2.*(E_p1 + E_p2)*pp*I6 );
double Vtt_66 = mn * ( (E_p2*P + E_p1*(3.*sqr_p2 - sqr_p1))*I3_iv - 2.*(E_p1 + E_p2)*pp*I6 );

Vtt = coeff*parameter[12]*parameter[14]*parameter[14]*(J*Vtt_12 + (J+1)*Vtt_34 + sqrt_j*(Vtt_55 + Vtt_66)) / (2*J+1);

Vtt_cor = coeff*parameter[12]*parameter[14]*parameter[14]*(E_p1 - E_p2)*(E_p1 - E_p2)*((EE - sqr_mn)*Qn4_1iv - (4.*J+1)*pp*Qn4_0 ) / (2.*J+1); 

Vtt = (Vtt_cor - Vtt)/(16.0*pi*pi*EE*sqr_mn);

                 //total potential
Vtotal = Vps + Vs + Vvv + Vvt + Vtt;

return Vtotal; 
}
//-------------(j-1)(j+1)--------------------------------------------------------------------------------------------------------------------------------------------------------- 
double V2(double p1,double p2, double parameter[], int J, int T)
{
                  //intermediate variables
double coeff;
double ms_is,gs_is,cutoffs_is;
double sqr_p1 = p1*p1; 
double sqr_p2 = p2*p2;
double sqr_mn = mn*mn;
double E_p1 = sqrt(sqr_p1 + sqr_mn);
double E_p2 = sqrt(sqr_p2 + sqr_mn);
double EE = E_p1*E_p2;
double P = sqr_p1 + sqr_p2;
double pp = p1*p2;
double EE3 = 3.*EE + sqr_mn;
double sqrt_j = sqrt((double)J*(J+1));
double Vps,Vs,Vvv,Vvt,Vtt_cor,Vtt,Vtotal;

                  //isospin dependence
if(T == 0) {ms_is = parameter[15]; gs_is = parameter[6]; cutoffs_is = parameter[7]; coeff = -3.0;} 
else {ms_is = parameter[16]; gs_is = parameter[8]; cutoffs_is = parameter[9]; coeff = 1.0;}

                  //ps contribution
Vps = (E_p1 - mn) * (E_p2 + mn) * (coeff*parameter[0]*Qn2(sqr_p1,sqr_p2,pp,mps_iv,parameter[1],J+1,1) + parameter[2]*Qn2(sqr_p1,sqr_p2,pp,mps_is,parameter[3],J+1,1))

     + (E_p1 + mn) * (E_p2 - mn) * (coeff*parameter[0]*Qn2(sqr_p1,sqr_p2,pp,mps_iv,parameter[1],J-1,1) + parameter[2]*Qn2(sqr_p1,sqr_p2,pp,mps_is,parameter[3],J-1,1))

     - 2*pp * (coeff*parameter[0]*Qn2(sqr_p1,sqr_p2,pp,mps_iv,parameter[1],J,1) + parameter[2]*Qn2(sqr_p1,sqr_p2,pp,mps_is,parameter[3],J,1));

Vps = - sqrt_j*Vps/(4.0*pi*pi*EE*(2*J+1));

                  //s contribution
int JJ = (2*J+1)*(2*J+1);

Vs = (E_p1-mn)*(E_p2-mn)*( coeff*parameter[4]*(Qn2(sqr_p1,sqr_p2,pp,ms_iv,parameter[5],J-1,1) - Qn2(sqr_p1,sqr_p2,pp,ms_iv,parameter[5],J+1,1)) 
						  
						  + gs_is*(Qn2(sqr_p1,sqr_p2,pp,ms_is,cutoffs_is,J-1,1) - Qn2(sqr_p1,sqr_p2,pp,ms_is,cutoffs_is,J+1,1)) );						   

Vs = - sqrt_j*Vs/(4.0*pi*pi*EE*JJ);

                                    //v contribution
double Qn4_0 = Qn4(sqr_p1,sqr_p2,pp,mv_iv,parameter[13],J,1);
double Qn4_1iv = Qn4(sqr_p1,sqr_p2,pp,mv_iv,parameter[13],J-1,1);
double Qn4_2iv = Qn4(sqr_p1,sqr_p2,pp,mv_iv,parameter[13],J+1,1);
double Qn4_1is = Qn4(sqr_p1,sqr_p2,pp,mv_is,parameter[11],J-1,1);
double Qn4_2is = Qn4(sqr_p1,sqr_p2,pp,mv_is,parameter[11],J+1,1);
double I1_is = (J*Qn4_1is + (J+1)*Qn4_2is) / (2*J+1);
double I2_is = ((J+1)*Qn4_1is + J*Qn4_2is) / (2*J+1);
double I1_iv = (J*Qn4_1iv + (J+1)*Qn4_2iv) / (2*J+1);
double I2_iv = ((J+1)*Qn4_1iv + J*Qn4_2iv) / (2*J+1);
double I3_iv = sqrt_j*(Qn4_2iv - Qn4_1iv) / (2*J+1);

double Qn4_3iv = Qn4(sqr_p1,sqr_p2,pp,mv_iv,parameter[13],J-2,1);
double Qn4_4iv = Qn4(sqr_p1,sqr_p2,pp,mv_iv,parameter[13],J+2,1);
double I4 = ( J*(J-1)*Qn4_3iv/(2*J-1) + (2*J*J*(2*J+3)-1)*Qn4_0/((2*J-1)*(2*J+3)) + (J+1)*(J+2)*Qn4_4iv/(2*J+3) ) / (2*J+1);
double I5 = ( (J*J-1)*Qn4_3iv/(2*J-1) + 2*J*(J+1)*(2*J+1)*Qn4_0/((2*J-1)*(2*J+3)) + J*(J+2)*Qn4_4iv/(2*J+3) ) / (2*J+1);
double I6 = - sqrt_j*( (J-1)*Qn4_3iv/(2*J-1) + (2*J+1)*Qn4_0/((2*J-1)*(2*J+3)) - (J+2)*Qn4_4iv/(2*J+3) ) / (2*J+1);

//vv
Vvv = pp * (parameter[10]*Qn4(sqr_p1,sqr_p2,pp,mv_is,parameter[11],J,1) + coeff*parameter[12]*Qn4_0)

     + sqr_mn * (parameter[10]*I1_is + coeff*parameter[12]*I1_iv) - EE * (parameter[10]*I2_is + coeff*parameter[12]*I2_iv)

	 + mn * (J*E_p2 - (J+1)*E_p1) * ( parameter[10]*(Qn4_2is - Qn4_1is) + coeff*parameter[12]*(Qn4_2iv - Qn4_1iv) ) / (2*J+1);

Vvv = - sqrt_j*Vvv/(2.0*pi*pi*EE*(2*J+1));  

//vt
double Vvt_12 = mn * (6.*pp*Qn4_0 - 3.*P*I1_iv);
double Vvt_34 = mn * (2.*pp*Qn4_0 - P*I2_iv);
double Vvt_55 = (E_p1*sqr_p2 + 3.*E_p2*sqr_p1) * I3_iv;
double Vvt_66 = (E_p2*sqr_p1 + 3.*E_p1*sqr_p2) * I3_iv;

Vvt = coeff*parameter[12]*parameter[14]*(sqrt_j*(Vvt_12 - Vvt_34) -J*Vvt_55 + (J+1)*Vvt_66) / (2*J+1);

Vvt = - Vvt/(4.0*pi*pi*EE*mn);

//tt
double Vtt_12 = (4.*sqr_mn - 3.*P)*pp*Qn4_0 + (6.*pp*pp - P*(EE + 3.*sqr_mn))*I1_iv + 2.*(EE + sqr_mn)*pp*I4;
double Vtt_34 = -(P + 4.*EE)*pp*Qn4_0 - 2.*pp*pp*I1_iv + (4.*pp*pp + P*(EE - sqr_mn))*I2_iv + 2.*(EE + sqr_mn)*pp*I5;
double Vtt_55 = mn * ( (E_p1*P + E_p2*(3.*sqr_p1 - sqr_p2))*I3_iv - 2.*(E_p1 + E_p2)*pp*I6 );
double Vtt_66 = mn * ( (E_p2*P + E_p1*(3.*sqr_p2 - sqr_p1))*I3_iv - 2.*(E_p1 + E_p2)*pp*I6 );

Vtt = coeff*parameter[12]*parameter[14]*parameter[14]*(sqrt_j*(Vtt_12 - Vtt_34) -J*Vtt_55 + (J+1)*Vtt_66) / (2*J+1);

Vtt_cor = coeff*parameter[12]*parameter[14]*parameter[14]*sqrt_j*(E_p1 - E_p2)*(E_p1 - E_p2)*((E_p1 - mn)*(E_p2 + mn)*Qn4_2iv 
																					
										             + (E_p1 + mn)*(E_p2 - mn)*Qn4_1iv + 2.*pp*Qn4_0) / (2*J+1);

Vtt = - (Vtt_cor + Vtt)/(16.0*pi*pi*EE*sqr_mn); 

                 //total potential
Vtotal = Vps + Vs + Vvv + Vvt + Vtt;

return Vtotal; 
}

//--------------(j+1)(j+1)-------------------------------------------------------------------------------------------------------------------------------------------------------
double V4(double p1,double p2, double parameter[], int J, int T)
{
                  //intermediate variables
double coeff;
double ms_is,gs_is,cutoffs_is;
double sqr_p1 = p1*p1; 
double sqr_p2 = p2*p2;
double sqr_mn = mn*mn;
double E_p1 = sqrt(sqr_p1 + sqr_mn);
double E_p2 = sqrt(sqr_p2 + sqr_mn);
double EE = E_p1*E_p2;
double P = sqr_p1 + sqr_p2;
double pp = p1*p2;
double EE3 = 3.*EE + sqr_mn;
double sqrt_j = sqrt((double)J*(J+1));
double Vps,Vs,Vvv,Vvt,Vtt_cor,Vtt,Vtotal;

                  //isospin dependence
if(T == 0) {ms_is = parameter[15]; gs_is = parameter[6]; cutoffs_is = parameter[7]; coeff = -3.0;} 
else {ms_is = parameter[16]; gs_is = parameter[8]; cutoffs_is = parameter[9]; coeff = 1.0;}

                  //ps contribution
Vps = (EE - sqr_mn) * (coeff*parameter[0]*Qn2(sqr_p1,sqr_p2,pp,mps_iv,parameter[1],J+1,1) + parameter[2]*Qn2(sqr_p1,sqr_p2,pp,mps_is,parameter[3],J+1,1))

             - pp * (coeff*parameter[0]*Qn2(sqr_p1,sqr_p2,pp,mps_iv,parameter[1],J,1) + parameter[2]*Qn2(sqr_p1,sqr_p2,pp,mps_is,parameter[3],J,1));

Vps = - Vps/(4.0*pi*pi*EE*(2*J+1));

                  //s contribution
int JJ = (2*J+1)*(2*J+1);

Vs = - (EE + sqr_mn) * (coeff*parameter[4]*Qn2(sqr_p1,sqr_p2,pp,ms_iv,parameter[5],J+1,1) + gs_is*Qn2(sqr_p1,sqr_p2,pp,ms_is,cutoffs_is,J+1,1))

     + pp * JJ * (coeff*parameter[4]*Qn2(sqr_p1,sqr_p2,pp,ms_iv,parameter[5],J,1) + gs_is*Qn2(sqr_p1,sqr_p2,pp,ms_is,cutoffs_is,J,1))

	 -2*J*(J+1) * ( (E_p1-mn)*(E_p2-mn)*(coeff*parameter[4]*Qn2(sqr_p1,sqr_p2,pp,ms_iv,parameter[5],J-1,1) + gs_is*Qn2(sqr_p1,sqr_p2,pp,ms_is,cutoffs_is,J-1,1))

	               + (E_p1+mn)*(E_p2+mn)*(coeff*parameter[4]*Qn2(sqr_p1,sqr_p2,pp,ms_iv,parameter[5],J+1,1) + gs_is*Qn2(sqr_p1,sqr_p2,pp,ms_is,cutoffs_is,J+1,1)) ); 

Vs = - Vs/(4.0*pi*pi*EE*JJ);

                                    //v contribution
double Qn4_0 = Qn4(sqr_p1,sqr_p2,pp,mv_iv,parameter[13],J,1);
double Qn4_1iv = Qn4(sqr_p1,sqr_p2,pp,mv_iv,parameter[13],J-1,1);
double Qn4_2iv = Qn4(sqr_p1,sqr_p2,pp,mv_iv,parameter[13],J+1,1);
double Qn4_1is = Qn4(sqr_p1,sqr_p2,pp,mv_is,parameter[11],J-1,1);
double Qn4_2is = Qn4(sqr_p1,sqr_p2,pp,mv_is,parameter[11],J+1,1);
double I1_is = (J*Qn4_1is + (J+1)*Qn4_2is) / (2*J+1);
double I2_is = ((J+1)*Qn4_1is + J*Qn4_2is) / (2*J+1);
double I1_iv = (J*Qn4_1iv + (J+1)*Qn4_2iv) / (2*J+1);
double I2_iv = ((J+1)*Qn4_1iv + J*Qn4_2iv) / (2*J+1);
double I3_iv = sqrt_j*(Qn4_2iv - Qn4_1iv) / (2*J+1);

double Qn4_3iv = Qn4(sqr_p1,sqr_p2,pp,mv_iv,parameter[13],J-2,1);
double Qn4_4iv = Qn4(sqr_p1,sqr_p2,pp,mv_iv,parameter[13],J+2,1);
double I11 = ((J+1)*Qn4_0 + (J+2)*Qn4_4iv) / (2*J+3);
double I4 = ( J*(J-1)*Qn4_3iv/(2*J-1) + (2*J*J*(2*J+3)-1)*Qn4_0/((2*J-1)*(2*J+3)) + (J+1)*(J+2)*Qn4_4iv/(2*J+3) ) / (2*J+1);
double I5 = ( (J*J-1)*Qn4_3iv/(2*J-1) + 2*J*(J+1)*(2*J+1)*Qn4_0/((2*J-1)*(2*J+3)) + J*(J+2)*Qn4_4iv/(2*J+3) ) / (2*J+1);
double I6 = - sqrt_j*( (J-1)*Qn4_3iv/(2*J-1) + (2*J+1)*Qn4_0/((2*J-1)*(2*J+3)) - (J+2)*Qn4_4iv/(2*J+3) ) / (2*J+1);

//vv
Vvv = (3*J+2) * pp * (parameter[10]*Qn4(sqr_p1,sqr_p2,pp,mv_is,parameter[11],J,1) + coeff*parameter[12]*Qn4_0)

     + (J+1) * sqr_mn * (parameter[10]*I1_is + coeff*parameter[12]*I1_iv) + J * EE * (parameter[10]*I2_is + coeff*parameter[12]*I2_iv)

	 + J*(J+1) * mn * (E_p1 + E_p2) * ( parameter[10]*(Qn4_2is - Qn4_1is) + coeff*parameter[12]*(Qn4_2iv - Qn4_1iv) ) / (2*J+1);

Vvv = - Vvv/(2.0*pi*pi*EE*(2*J+1)); 

//vt
double Vvt_12 = mn * (6.*pp*Qn4_0 - 3.*P*I1_iv);
double Vvt_34 = mn * (2.*pp*Qn4_0 - P*I2_iv);
double Vvt_55 = (E_p1*sqr_p2 + 3.*E_p2*sqr_p1) * I3_iv;
double Vvt_66 = (E_p2*sqr_p1 + 3.*E_p1*sqr_p2) * I3_iv;

Vvt = coeff*parameter[12]*parameter[14]*((J+1)*Vvt_12 + J*Vvt_34 - sqrt_j*(Vvt_55 + Vvt_66)) / (2*J+1);

Vvt = - Vvt/(4.0*pi*pi*EE*mn);

//tt
double Vtt_12 = (4.*sqr_mn - 3.*P)*pp*Qn4_0 + (6.*pp*pp - P*(EE + 3.*sqr_mn))*I1_iv + 2.*(EE + sqr_mn)*pp*I4;
double Vtt_34 = -(P + 4.*EE)*pp*Qn4_0 - 2.*pp*pp*I1_iv + (4.*pp*pp + P*(EE - sqr_mn))*I2_iv + 2.*(EE + sqr_mn)*pp*I5;
double Vtt_55 = mn * ( (E_p1*P + E_p2*(3.*sqr_p1 - sqr_p2))*I3_iv - 2.*(E_p1 + E_p2)*pp*I6 );
double Vtt_66 = mn * ( (E_p2*P + E_p1*(3.*sqr_p2 - sqr_p1))*I3_iv - 2.*(E_p1 + E_p2)*pp*I6 );

Vtt = coeff*parameter[12]*parameter[14]*parameter[14]*((J+1)*Vtt_12 + J*Vtt_34 - sqrt_j*(Vtt_55 + Vtt_66)) / (2*J+1);

Vtt_cor = coeff*parameter[12]*parameter[14]*parameter[14]*(E_p1 - E_p2)*(E_p1 - E_p2)*( (EE - sqr_mn)*Qn4_2iv + (4*J+3)*pp*Qn4_0 ) / (2*J+1); 

Vtt = - (Vtt_cor + Vtt)/(16.0*pi*pi*EE*sqr_mn); 

                 //total potential
Vtotal = Vps + Vs + Vvv + Vvt + Vtt;

return Vtotal; 
}

//---------------------------------------------------------------------------------------------------------------------------------------------------------------------
void potential_matrix_coupled(double parameter[], double out_matrix[2*N][2*N], int J, int T)
{
for(int i = 0; i < N; i++)
for(int j = i; j < N; j++)
     {out_matrix[i][j] = V1(moment[i],moment[j],parameter,J,T);
      out_matrix[j][i] = out_matrix[i][j];
	  out_matrix[N+i][N+j] = V4(moment[i],moment[j],parameter,J,T);
      out_matrix[N+j][N+i] = out_matrix[N+i][N+j];}

for(int i = 0; i < N; i++)
for(int j = 0; j < N; j++)
    {out_matrix[i][N+j] = V2(moment[i],moment[j],parameter,J,T);
     out_matrix[N+i][j] = out_matrix[i][N+j];
    }
}

void coupled_ps_for_set(double parameter[], double energy[], double phase_shift[][3], int set, int J, int T)
{            
	              //intermediate variables
double pot_mtrx[2*N][2*N];
double W[N+1];
//double A[2*(N+1)][2*(N+1)],VST1[2*(N+1)],VST2[2*(N+1)]; //,RM1[2*(N+1)],RM2[2*(N+1)];
//int transpos[2*(N+1)],ERTYPE;
alglib::real_2d_array A;
A.setlength(2*(N+1),2*(N+1));
alglib::real_1d_array VST1, VST2;
VST1.setlength(2*(N+1)); 
VST2.setlength(2*(N+1)); 
double EA,FI,SN,AK2,EF,S1,P0;
double R11,R13,R31,R33,GG,U,B0,B1,B2,EPS_B,FJMIN_B,FJMAX_B,EPS_S,FJMIN_S,FJMAX_S;
int N1;

potential_matrix_coupled(parameter,pot_mtrx,J,T);

N1 = N + 1;
FI = pi/4.;

for(int s = 0; s < set; s++)
{
EA = energy[s]/hc; 
P0 = sqrt(mn*EA/2.);
AK2 = P0*P0;
	   
moment[N] = P0;

SN = 0.0;

for(int i = 0; i < N; i++)
for(int j = 0; j < N; j++)
     {A[i][j] = pot_mtrx[i][j];
      A[N+1+i][N+1+j] = pot_mtrx[N+i][N+j];}
      
for(int i = 0; i < N; i++)
for(int j = 0; j < N; j++)
    {A[i][N+1+j] = pot_mtrx[i][N+j];
     A[N+1+j][i] = pot_mtrx[N+i][j];}

for(int i = 0; i < N; i++) 
    {A[i][N] = V1(moment[i],moment[N],parameter,J,T);
     A[N][i] = A[i][N];

     A[N+1+i][2*N+1] = V4(moment[i],moment[N],parameter,J,T);
     A[2*N+1][N+1+i] = A[N+1+i][2*N+1];

     A[i][2*N+1] = V2(moment[i],moment[N],parameter,J,T);
     A[2*N+1][i] = A[i][2*N+1];

     A[N][N+1+i] = V2(moment[N],moment[i],parameter,J,T);
     A[N+1+i][N] = A[N][N+1+i];}

A[N][N] = V1(moment[N],moment[N],parameter,J,T);
A[2*N+1][2*N+1] = V4(moment[N],moment[N],parameter,J,T);
A[N][2*N+1] = V2(moment[N],moment[N],parameter,J,T);
A[2*N+1][N] = A[N][2*N+1];

for(int i = 0; i < N; i++)
    {W[i] = wght[i] / (moment[i]*moment[i] - AK2);
     SN = SN + W[i];}
W[N] = - SN;

for(int i = 0; i < N1; i++)
  {EF = moment[i]*moment[i]*W[i]*( sqrt(mn*mn + moment[i]*moment[i]) + sqrt(mn*mn + AK2) ) / 2.;
   for(int j = 0; j < N1; j++)     
      {S1 = EF*A[j][i];
      if (j == i)
         A[j][i] = 1.0 + S1;
	  else
		 A[j][i] = S1;

	  S1 = EF*A[N+1+j][N+1+i];
      if (j == i)
         A[N+1+j][N+1+i] = 1.0 + S1;
	  else
		 A[N+1+j][N+1+i] = S1;

	  S1 = EF*A[j][N+1+i];
	  A[j][N+1+i] = S1;

	  S1 = EF*A[N+1+j][i];
	  A[N+1+j][i] = S1; }}

for(int i = 0; i < N1; i++)
    {VST1[i] = V1(moment[i],moment[N],parameter,J,T);
     VST1[N1+i] = V2(moment[N],moment[i],parameter,J,T);}

for(int i = 0; i < N1; i++)
    {VST2[i] = V2(moment[i],moment[N],parameter,J,T);
     VST2[N1+i] = V4(moment[i],moment[N],parameter,J,T);}

//double A1[4*(N+1)*(N+1)];
//double RM1[2*(N+1)], RM2[2*(N+1)];
alglib::real_1d_array RM1, RM2;
RM1.setlength(2*(N+1));
RM2.setlength(2*(N+1));
/*for(int i = 0; i < 2*(N+1); i++)
  for(int j = 0; j < 2*(N+1); j++)
    A1[2*i*(N+1)+j] = A[i][j];

imsl_d_lin_sol_gen(2*(N+1), A1, VST1, IMSL_RETURN_USER, RM1, 0);
imsl_d_lin_sol_gen(2*(N+1), A1, VST2, IMSL_RETURN_USER, RM2, 0);*/
alglib::ae_int_t info;
alglib::densesolverreport rep;
alglib::rmatrixsolve(A,2*(N+1),VST1,info,rep,RM1);
alglib::rmatrixsolve(A,2*(N+1),VST2,info,rep,RM2);


R11 = RM1[N];
R13 = RM1[2*N+1];
R31 = RM2[N];
R33 = RM2[2*N+1];

//            BLATT-BIDENHARN PARAMETRIZATION
EPS_B = atan( (R13+R31)/(R11-R33) ) / 2.;
U = -pi*P0*sqrt(mn*mn+AK2)/4.;                                        
FJMIN_B = atan( (R11+R33 + (R11-R33)/cos(2.*EPS_B) ) * U );
FJMAX_B = atan( (R11+R33 - (R11-R33)/cos(2.*EPS_B) ) * U );

//             STAPP at al. PARAMETRIZATION
B0 = sin(2*EPS_B)*sin(FJMIN_B-FJMAX_B);
B1 = sqrt(1-B0*B0);
EPS_S = atan(B0/B1)/2.;
B0 = sin(2.*EPS_S)/cos(2.*EPS_S)/sin(2.*EPS_B)*cos(2.*EPS_B);
B1 = sqrt(1-B0*B0);
B2 = atan(B0/B1);
FJMIN_S = (B2+FJMIN_B+FJMAX_B)/2.;
FJMAX_S = (FJMIN_B+FJMAX_B-B2)/2.;

//     transformation of phase shifts into degrees
GG = 180./pi;
EPS_B = EPS_B*GG;
FJMIN_B = FJMIN_B*GG;
FJMAX_B = FJMAX_B*GG;
EPS_S = EPS_S*GG;
FJMIN_S = FJMIN_S*GG;
FJMAX_S = FJMAX_S*GG;

phase_shift[s][0] = FJMIN_S;
phase_shift[s][1] = FJMAX_S;
phase_shift[s][2] = EPS_S;
}

}
void coupled_phase_array(double parameter[], double energy[], double phases_array[])
{
	int count = 0;
	double ps_3S1[N2][3], ps_3P2[N2][3];
	coupled_ps_for_set(parameter, energy, ps_3S1, N2, 1, 0);
	coupled_ps_for_set(parameter, energy, ps_3P2, N2, 2, 1);
	for (int i = 0; i < N2; i++)
	{
		if (ps_3S1[i][0] < 0 && count == 0){
			phases_array[i] = ps_3S1[i][0] + 180;
		}
		else{
			phases_array[i] = ps_3S1[i][0];
			count += 1; 
		}
		phases_array[N2 + i] = ps_3S1[i][1];
		phases_array[2 * N2 + i] = ps_3P2[i][0];
		phases_array[3 * N2 + i] = ps_3P2[i][1];
	}
	
}
void coupled_UCT(double variables[], double energy[], double ps[])
{

points();
coupled_phase_array(variables, energy, ps);

}

