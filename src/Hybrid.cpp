#include <algorithm>
#include <mpi.h>
#include <stdio.h>
#include <cstring>
#include <time.h>
#include <math.h>
#include <iostream>
#include <cmath>
#include <cstdlib>
#include <fstream>
#include <string>
#include <iomanip>
#include "noncoupled_UCT.h"
#include "coupled_UCT.h"
#include <math.h>
using namespace std;
# include "rand/sobol.hpp"
# include "rand/faure.hpp"

#define hc 197.3269718
#define pi 3.141592653589793

/*RoyOfParticles*/
/*GENERAL*/
#define DIMENSION 17 

double time1, time2,duration,global;
double moment_noncoupled[N+1];
double wght_noncoupled[N];
alglib::real_1d_array x;
alglib::real_1d_array w;
alglib::ae_int_t n_noncoupled = N;
int n2_noncoupled = 1;
alglib::ae_int_t info_noncoupled = n2_noncoupled;

double moment[N + 1];
double wght[N];
alglib::real_1d_array gause_x;
alglib::real_1d_array gause_w;
alglib::ae_int_t n = N;
int n2 = 1;
alglib::ae_int_t info = n2;

int period = 10;
int max_num_of_energy = 16;
double GLOBAL = 0.3;
double PERSONAL = 0.1;
double INERT = 0.0;
double max_inert = 0.9;
double m = 4.758187;
double hc_loc = 197.3269718;
int Number = 8;	
int num_of_steps = 20;
double deflection = pow(10, - 9);
/*SWARM*/
int num_of_iteration = 100; 

/*NelderNid*/
double beta = 0.5;
double alpha_1 = 1;
double gamma_1 = 2;
double dispersia = pow(10, -10);
int max_counter_neld = 1;

double *errors;
double **velocity;
float **tmp_swar;
float **tmp_vel;
double **swarm;
double **BestPers;
double BestGlob[DIMENSION + 1];
int counter = 1;
/*RoyOfParticles*/


double X[DIMENSION][(DIMENSION + 1)];
double f[(DIMENSION + 1)];
double x_c[DIMENSION];
double x_r[DIMENSION];
double x_s[DIMENSION];
double x_e[DIMENSION];
double x_l[DIMENSION];
double x_h[DIMENSION];
double x_g[DIMENSION];
int counter_neld = 1;
int max1 = 0, min1 = 0, max2 = 0;
double LOW_BOUNDS[17];
double UP_BOUNDS[17];

double *energy;
int num_of_energy = 16;
double* phases_model;
bool flag = false;
int num_of_generator = 0;
struct StaticBlock {
	StaticBlock() {
		ifstream config;
		config.open("./IN/config.txt");
		string value;
		string key;
		char temp[256];
		if (!config.is_open()) cout << "error, config.txt is not found";
		while (config >> key) {
			config >> value;
			strcpy(temp, value.c_str());
      if(key.compare("GLOBAL") == 0){
       GLOBAL = atof(temp);
      }
      else if(key.compare("PERSONAL") == 0){
       PERSONAL = atof(temp);
      }
      else if(key.compare("INERT") == 0){
       INERT = atof(temp);
      }
      else if(key.compare("max_INERT") == 0){
        max_inert = atof(temp);
      }
      else if(key.compare("num_of_individs") == 0){
       Number = atoi(temp);
      }
      else if(key.compare("num_of_iteration") == 0){
       num_of_iteration = atoi(temp);
      }
      else if(key.compare("beta") == 0){
       beta = atof(temp);
      }
      
      else if(key.compare("alpha") == 0){
       alpha_1 = atof(temp);
      }
      else if(key.compare("gamma") == 0){
        gamma_1 = atof(temp);
      }
      else if(key.compare("period") == 0){
        period = atoi(temp);
      }
      else if(key.compare("variance") == 0){
       dispersia = pow(10, atoi(temp));
      }
      else if(key.compare("deflection") == 0){
       deflection = pow(10, atoi(temp));
      }
      else if(key.compare("num_of_steps") == 0){
       num_of_steps = atoi(temp);
      }
      else if(key.compare("max_of_iter_neld") == 0){
       max_counter_neld = atoi(temp);
      }
      else if(key.compare("flag") == 0){
         if (atoi(temp) != 0) {
			      flag = true;
         }
      }
      else if(key.compare("num_of_generator") == 0){
        num_of_generator = atoi(temp);
      }	
		}
		config.close();
		errors = new double [num_of_steps];
		swarm = new double *[Number];
		tmp_vel = new float *[Number];
		velocity = new double *[Number];
		tmp_swar = new float *[Number];
		BestPers = new double *[Number];
		for (int k = 0; k < Number; k++) {
			swarm[k] = new double[DIMENSION];
			tmp_vel[k] = new float[DIMENSION];
			velocity[k] = new double[DIMENSION];
			tmp_swar[k] = new float[DIMENSION];
			BestPers[k] = new double[DIMENSION + 1];
		}
		config.close();

		ifstream bound;
		bound.open("./IN/bounds.txt");
		string word1;
		string word2;
		int flag_bound = 0;
		char buffer[256];
		if (!bound.is_open()) cout << "error, bounds.txt is not founded";
		while (bound >> word1) {
			bound >> word2;
			strcpy(buffer, word1.c_str());
			LOW_BOUNDS[flag_bound] = atof(buffer);
			strcpy(buffer, word2.c_str());
			UP_BOUNDS[flag_bound] = atof(buffer);
			flag_bound++;
		}
		bound.close();

		ifstream energy_stream;
		energy_stream.open("./IN/energy_in.txt");
		string energy_str;
		char temp_energy[256];
		int flag_tmp = 0;
		if (!energy_stream.is_open()) cout << "error, energy_in.txt is not founded";
		energy_stream >> energy_str;
		strcpy(temp_energy, energy_str.c_str());
		num_of_energy = atoi(temp_energy);
		energy = new double[num_of_energy];
		phases_model = new double[num_of_energy * 10];
		while (energy_stream >> energy_str) {
			strcpy(temp_energy, energy_str.c_str());
			energy[flag_tmp] = atof(temp_energy);
			flag_tmp++;
			if (flag_tmp >= num_of_energy)
				break;
		}
		energy_stream.close();

		ifstream wjc;
		wjc.open("./IN/ps_ref_wjc1.txt");
		char buffer2[256];
		if (!wjc.is_open()) cout << "error, wjc is not founded";
		int count_wjc = 0;
		while (wjc >> word1) {
			strcpy(buffer2, word1.c_str());
			phases_model[count_wjc] = atof(buffer2);
			count_wjc++;
			if (count_wjc >= num_of_energy * 10) break;
		}
		wjc.close();
	}
};
static StaticBlock staticBlock;

 

double get_r(double phase1, double phase2) {
	return 4 / (m * (0.01/hc_loc  - 0.03 / hc_loc)) * sqrt(m /(2 * hc_loc)) * (sqrt(0.01)* cos(phase1 * pi / 180) / sin(phase1 * pi / 180) - sqrt(0.03)* cos(phase2 * pi / 180) / sin(phase2 * pi / 180));
}

double get_a(double phase, double E, double r) {
	return 1 / (r * E * m / (4 * hc_loc) - sqrt(E * m / (2 * hc_loc))*cos(phase * pi / 180) / sin(phase * pi / 180));
}

double get_Ed(double r, double a) {
	return hc_loc / m * pow((1 - sqrt(1 - 2 * r / a)) / r, 2);
}


double calculate(double var[])
{
	double *ps_noncoupled = new double[6 * num_of_energy];
	double *ps_coupled = new double[4 * num_of_energy];
	double funct = 0;
	double x[DIMENSION];
	for (int j = 0; j < DIMENSION; j++) {
		if ((j) % 2 != 0) {
			x[j] = var[j] / hc;
		}
		else {
			x[j] = var[j];
		}
	}
	x[16] /= hc;
	noncoupled_UCT(x, energy, ps_noncoupled);
	coupled_UCT(x, energy, ps_coupled);

	for (int v = 0; v < 6 * num_of_energy; v++) {
		funct += pow(ps_noncoupled[v] - phases_model[v], 2);
	}

	for (int v = 0; v < 4 * num_of_energy; v++) {
		funct += pow(ps_coupled[v] - phases_model[v + 6 * num_of_energy], 2);
	}

	fstream phases_at_point;
	phases_at_point.open("./OUT/phases_at_point.txt", ios::out);
	if (phases_at_point) {
	 phases_at_point << "value_of_function: " << funct << '\n';
   phases_at_point << '\n';
   phases_at_point << "noncoupled_phases: " << '\n';
		for (int i = 0; i < num_of_energy * 10; ++i)
		{
			if (i < 6 * num_of_energy) {
				phases_at_point << fixed << setprecision(15) << ps_noncoupled[i] - phases_model[i] << '\t';
        if(i+1 == num_of_energy|| i+1 == 2 * num_of_energy || i+1 == 3 * num_of_energy || i+1 == 4 * num_of_energy || i+1 == 5 * num_of_energy){
        phases_at_point << '\n';
        }
			}
			else {
				if (i == 6 * num_of_energy) {
        phases_at_point << '\n';
          phases_at_point << '\n';
					phases_at_point << "coupled_phases: " << '\n';
				}
				phases_at_point << fixed << setprecision(15) << ps_coupled[i - 6 * num_of_energy] - phases_model[i] << '\t';
        if(i+1 == 7*num_of_energy|| i+1 == 8 * num_of_energy || i+1 == 9 * num_of_energy){
        phases_at_point << '\n';
        }
			}
		}
	}
  phases_at_point << '\n';
  phases_at_point << '\n';
	double r_s = get_r(ps_noncoupled[0], ps_noncoupled[1]);
	double r_t = get_r(phases_model[90], phases_model[91]);

	double a_s = get_a(ps_noncoupled[0], 0.01, r_s);
	double a_t = get_a(ps_coupled[0], 0.01, r_t);
	double E_d = get_Ed(r_t, a_t);
	phases_at_point << "a_s, r_s, a_t, r_t, E_d: " << '\n';
	phases_at_point << fixed << setprecision(6) << a_s << '\n';
	phases_at_point << fixed << setprecision(6) << r_s << '\n';
	phases_at_point << fixed << setprecision(6) << a_t << '\n';
	phases_at_point << fixed << setprecision(6) << r_t << '\n';
	phases_at_point << fixed << setprecision(6) << E_d << '\n';
	phases_at_point.close();
  delete[] ps_noncoupled;
	delete[] ps_coupled;
	return funct;	
}

double calculate(int i)
{
	double *ps_noncoupled = new double[6 * num_of_energy];
	double *ps_coupled = new double[4 * num_of_energy];
	double funct = 0;
	double x[DIMENSION];
	for (int j = 0; j < DIMENSION; j++) {
		if ((j) % 2 != 0) {
			x[j] = X[j][i] / hc;
		}
		else {
			x[j] = X[j][i];
		}
	}
	x[16] /= hc;
	noncoupled_UCT(x, energy, ps_noncoupled);
	coupled_UCT(x, energy, ps_coupled);

	 for (int v = 0; v < 6 * num_of_energy; v++) {
if(v < 2){
funct += 10000  * pow(ps_noncoupled[v] - phases_model[v], 2);
}
if(v >= 3 * num_of_energy && v < 5 * num_of_energy){
funct += 100  * pow(ps_noncoupled[v] - phases_model[v], 2);
}
else{
		funct += pow(ps_noncoupled[v] - phases_model[v], 2);
		}
	}

	for (int v = 0; v < 4 * num_of_energy; v++) {
	if(v < 2){
		funct += 10000 * pow(ps_coupled[v] - phases_model[v + 6 * num_of_energy], 2);
	}
 if(v >= 2 * num_of_energy && v < 3 * num_of_energy){
 funct += 100 * pow(ps_coupled[v] - phases_model[v + 6 * num_of_energy], 2);
 }
	else{
		funct += pow(ps_coupled[v] - phases_model[v + 6 * num_of_energy], 2);
	}
	}
  delete[] ps_noncoupled;
	delete[] ps_coupled;
	return funct;
}

double calculating(double *y)
{
	double *ps_noncoupled = new double[6 * num_of_energy];
	double *ps_coupled = new double[4 * num_of_energy];
	double funct = 0;
	double x[DIMENSION];
	for (int j = 0; j < DIMENSION; j++) {
		if ((j) % 2 != 0) {
			x[j] = y[j] / hc;
		}
		else {
			x[j] = y[j];
		}
	}
	x[16] /= hc;
	noncoupled_UCT(x, energy, ps_noncoupled);
	coupled_UCT(x, energy, ps_coupled);
	
	 for (int v = 0; v < 6 * num_of_energy; v++) {
if(v < 2){
funct += 10000  * pow(ps_noncoupled[v] - phases_model[v], 2);
}
if(v >= 3 * num_of_energy && v < 5 * num_of_energy){
funct += 100  * pow(ps_noncoupled[v] - phases_model[v], 2);
}
else{
		funct += pow(ps_noncoupled[v] - phases_model[v], 2);
		}
	}

	for (int v = 0; v < 4 * num_of_energy; v++) {
	if(v < 2){
		funct += 10000 * pow(ps_coupled[v] - phases_model[v + 6 * num_of_energy], 2);
	}
 if(v >= 2 * num_of_energy && v < 3 * num_of_energy){
 funct += 100 * pow(ps_coupled[v] - phases_model[v + 6 * num_of_energy], 2);
 }
	else{
		funct += pow(ps_coupled[v] - phases_model[v + 6 * num_of_energy], 2);
	}
	}
  delete[] ps_noncoupled;
	delete[] ps_coupled;
	return funct;
}

double calculate_swarm(int i)
{
  double *ps_noncoupled = new double[6 * num_of_energy];
  double *ps_coupled = new double[4 * num_of_energy];
	double funct = 0;
	double x[DIMENSION];
	for (int j = 0; j < DIMENSION; j++) {
		if ((j) % 2 != 0) {
			x[j] = swarm[i][j] / hc;
		}
		else {
			x[j] = swarm[i][j];
		}
	}
	x[16] /= hc;
	noncoupled_UCT(x, energy, ps_noncoupled);
	coupled_UCT(x, energy, ps_coupled);
	
	 for (int v = 0; v < 6 * num_of_energy; v++) {
if(v < 2){
funct += 10000  * pow(ps_noncoupled[v] - phases_model[v], 2);
}
if(v >= 3 * num_of_energy && v < 5 * num_of_energy){
funct += 100  * pow(ps_noncoupled[v] - phases_model[v], 2);
}
else{
		funct += pow(ps_noncoupled[v] - phases_model[v], 2);
		}
	}

	for (int v = 0; v < 4 * num_of_energy; v++) {
	if(v < 2){
		funct += 10000 * pow(ps_coupled[v] - phases_model[v + 6 * num_of_energy], 2);
	}
 if(v >= 2 * num_of_energy && v < 3 * num_of_energy){
 funct += 100 * pow(ps_coupled[v] - phases_model[v + 6 * num_of_energy], 2);
 }
	else{
		funct += pow(ps_coupled[v] - phases_model[v + 6 * num_of_energy], 2);
	}
	}
 
  delete[] ps_noncoupled;
	delete[] ps_coupled;
	return funct;
}

double calculate_swarm_parallel(int i, double *a){
  double *ps_noncoupled = new double[6 * num_of_energy];
  double *ps_coupled = new double[4 * num_of_energy];
	double funct = 0;
  int count_a = i * DIMENSION;
	double x[DIMENSION];
	for (int j = 0; j < DIMENSION; j++) {
		if ((j) % 2 != 0) {
			x[j] = a[count_a] / hc;
		}
		else {
			x[j] = a[count_a] ;
		}
   count_a++;
	}
	x[16] /= hc;
	noncoupled_UCT(x, energy, ps_noncoupled);
	coupled_UCT(x, energy, ps_coupled);
	
	 for (int v = 0; v < 6 * num_of_energy; v++) {
if(v < 2){
funct += 10000  * pow(ps_noncoupled[v] - phases_model[v], 2);
}
if(v >= 3 * num_of_energy && v < 5 * num_of_energy){
funct += 100  * pow(ps_noncoupled[v] - phases_model[v], 2);
}
else{
		funct += pow(ps_noncoupled[v] - phases_model[v], 2);
		}
	}

	for (int v = 0; v < 4 * num_of_energy; v++) {
	if(v < 2){
		funct += 10000 * pow(ps_coupled[v] - phases_model[v + 6 * num_of_energy], 2);
	}
 if(v >= 2 * num_of_energy && v < 3 * num_of_energy){
 funct += 100 * pow(ps_coupled[v] - phases_model[v + 6 * num_of_energy], 2);
 }
	else{
		funct += pow(ps_coupled[v] - phases_model[v + 6 * num_of_energy], 2);
	}
	}
  delete[] ps_noncoupled;
	delete[] ps_coupled;
	return funct;




}

double *Lrand(float min, float max) {
	double *tmp = i8_sobol_generate(1, Number, 0);
	for (int i = 0; i < Number; i++) {
			tmp[i] = min + tmp[i] * (max - min);
	}
	return tmp;
	delete[] tmp;
}



double *Lrand3(float min, float max) {
	double *tmp = faure_generate(1, Number, 0);
	for (int i = 0; i < Number; i++) {
			tmp[i] = min + tmp[i] * (max - min);
	}
	return tmp;
	delete[] tmp;
}

float *Lrand_other(float min, float max) {
	float *tmp = i4_sobol_generate(DIMENSION, Number, 0);
	for (int i = 0; i < DIMENSION * Number; i++) {
			tmp[i] = min + tmp[i];
	}
	return tmp;
	delete[] tmp;
}

void init_swarm() //Generate initial positions and directions
{
  
  
	double *swar1;
	double *swar2;
	double *swar3;
	double *swar4;
	double *swar5;
	double *swar6;
	double *swar7;
	double *swar8;
	double *swar9;
	double *swar10;
	double *swar11;
	double *swar12;
	double *swar13;
	double *swar14;
	double *swar15;
	double *swar16;
	double *swar17;
 
  double *vel1 = Lrand(LOW_BOUNDS[0], UP_BOUNDS[0]);
	double *vel2 = Lrand(LOW_BOUNDS[1], UP_BOUNDS[1]);
	double *vel3 = Lrand(LOW_BOUNDS[2], UP_BOUNDS[2]);
	double *vel4 = Lrand(LOW_BOUNDS[3], UP_BOUNDS[3]);
	double *vel5 = Lrand(LOW_BOUNDS[4], UP_BOUNDS[4]);
	double *vel6 = Lrand(LOW_BOUNDS[5], UP_BOUNDS[5]);
	double *vel7 = Lrand(LOW_BOUNDS[6], UP_BOUNDS[6]);
	double *vel8 = Lrand(LOW_BOUNDS[7], UP_BOUNDS[7]);
	double *vel9 = Lrand(LOW_BOUNDS[8], UP_BOUNDS[8]);
	double *vel10 = Lrand(LOW_BOUNDS[9], UP_BOUNDS[9]);
	double *vel11 = Lrand(LOW_BOUNDS[10], UP_BOUNDS[10]);
	double *vel12 = Lrand(LOW_BOUNDS[11], UP_BOUNDS[11]);
	double *vel13 = Lrand(LOW_BOUNDS[12], UP_BOUNDS[12]);
	double *vel14 = Lrand(LOW_BOUNDS[13], UP_BOUNDS[13]);
	double *vel15 = Lrand(LOW_BOUNDS[14], UP_BOUNDS[14]);
	double *vel16 = Lrand(LOW_BOUNDS[15], UP_BOUNDS[15]);
	double *vel17 = Lrand(LOW_BOUNDS[16], UP_BOUNDS[16]);
 
  if(num_of_generator == 2){
    swar1 = Lrand3(LOW_BOUNDS[0], UP_BOUNDS[0]);
	  swar2 = Lrand3(LOW_BOUNDS[1], UP_BOUNDS[1]);
	  swar3 = Lrand3(LOW_BOUNDS[2], UP_BOUNDS[2]);
	  swar4 = Lrand3(LOW_BOUNDS[3], UP_BOUNDS[3]);
	  swar5 = Lrand3(LOW_BOUNDS[4], UP_BOUNDS[4]);
	  swar6 = Lrand3(LOW_BOUNDS[5], UP_BOUNDS[5]);
	  swar7 = Lrand3(LOW_BOUNDS[6], UP_BOUNDS[6]);
    swar8 = Lrand3(LOW_BOUNDS[7], UP_BOUNDS[7]);
	  swar9 = Lrand3(LOW_BOUNDS[8], UP_BOUNDS[8]);
	  swar10 = Lrand3(LOW_BOUNDS[9], UP_BOUNDS[9]);
	  swar11 = Lrand3(LOW_BOUNDS[10], UP_BOUNDS[10]);
	  swar12 = Lrand3(LOW_BOUNDS[11], UP_BOUNDS[11]);
	  swar13 = Lrand3(LOW_BOUNDS[12], UP_BOUNDS[12]);
	  swar14 = Lrand3(LOW_BOUNDS[13], UP_BOUNDS[13]);
	  swar15 = Lrand3(LOW_BOUNDS[14], UP_BOUNDS[14]);
	  swar16 = Lrand3(LOW_BOUNDS[15], UP_BOUNDS[15]);
	  swar17 = Lrand3(LOW_BOUNDS[16], UP_BOUNDS[16]);
  }
  
  else{
    swar1 = Lrand(LOW_BOUNDS[0], UP_BOUNDS[0]);
	  swar2 = Lrand(LOW_BOUNDS[1], UP_BOUNDS[1]);
	  swar3 = Lrand(LOW_BOUNDS[2], UP_BOUNDS[2]);
	  swar4 = Lrand(LOW_BOUNDS[3], UP_BOUNDS[3]);
	  swar5 = Lrand(LOW_BOUNDS[4], UP_BOUNDS[4]);
	  swar6 = Lrand(LOW_BOUNDS[5], UP_BOUNDS[5]);
	  swar7 = Lrand(LOW_BOUNDS[6], UP_BOUNDS[6]);
    swar8 = Lrand(LOW_BOUNDS[7], UP_BOUNDS[7]);
	  swar9 = Lrand(LOW_BOUNDS[8], UP_BOUNDS[8]);
	  swar10 = Lrand(LOW_BOUNDS[9], UP_BOUNDS[9]);
	  swar11 = Lrand(LOW_BOUNDS[10], UP_BOUNDS[10]);
	  swar12 = Lrand(LOW_BOUNDS[11], UP_BOUNDS[11]);
	  swar13 = Lrand(LOW_BOUNDS[12], UP_BOUNDS[12]);
	  swar14 = Lrand(LOW_BOUNDS[13], UP_BOUNDS[13]);
	  swar15 = Lrand(LOW_BOUNDS[14], UP_BOUNDS[14]);
	  swar16 = Lrand(LOW_BOUNDS[15], UP_BOUNDS[15]);
	  swar17 = Lrand(LOW_BOUNDS[16], UP_BOUNDS[16]);
  }

  for (int i = 0; i < Number; i++) BestPers[i][DIMENSION] = RAND_MAX;


	/*for (size_t z = 0; z < Number; z ++) {
  std::swap(swar2[z], swar2[rand() % Number]);
  }*/
  random_shuffle(&swar1[0], &swar1[Number]);
  random_shuffle(&swar2[0], &swar2[Number]);
  random_shuffle(&swar3[0], &swar3[Number]);
  random_shuffle(&swar4[0], &swar4[Number]);
  random_shuffle(&swar5[0], &swar5[Number]);
  random_shuffle(&swar6[0], &swar6[Number]);
  random_shuffle(&swar7[0], &swar7[Number]);
  random_shuffle(&swar8[0], &swar8[Number]);
  random_shuffle(&swar9[0], &swar9[Number]);
  random_shuffle(&swar10[0], &swar10[Number]);
  random_shuffle(&swar11[0], &swar11[Number]);
  random_shuffle(&swar12[0], &swar12[Number]);
  random_shuffle(&swar13[0], &swar13[Number]);
  random_shuffle(&swar14[0], &swar14[Number]);
  random_shuffle(&swar15[0], &swar15[Number]);
  random_shuffle(&swar16[0], &swar16[Number]);
  random_shuffle(&swar17[0], &swar17[Number]);
  for (int z = 0; z < Number; z++) swarm[z][0] = swar1[z];
	for (int z = 0; z < Number; z++) swarm[z][1] = swar2[z];
	for (int z = 0; z < Number; z++) swarm[z][2] = swar3[z];
	for (int z = 0; z < Number; z++) swarm[z][3] = swar4[z];
	for (int z = 0; z < Number; z++) swarm[z][4] = swar5[z];
	for (int z = 0; z < Number; z++) swarm[z][5] = swar6[z];
	for (int z = 0; z < Number; z++) swarm[z][6] = swar7[z];
	for (int z = 0; z < Number; z++) swarm[z][7] = swar8[z];
	for (int z = 0; z < Number; z++) swarm[z][8] = swar9[z];
	for (int z = 0; z < Number; z++) swarm[z][9] = swar10[z];
	for (int z = 0; z < Number; z++) swarm[z][10] = swar11[z];
	for (int z = 0; z < Number; z++) swarm[z][11] = swar12[z];
	for (int z = 0; z < Number; z++) swarm[z][12] = swar13[z];
	for (int z = 0; z < Number; z++) swarm[z][13] = swar14[z];
	for (int z = 0; z < Number; z++) swarm[z][14] = swar15[z];
	for (int z = 0; z < Number; z++) swarm[z][15] = swar16[z];
	for (int z = 0; z < Number; z++) swarm[z][16] = swar17[z];
 
  /* for (int i = 0; i < Number; i++)
	{
		for (int a = 0; a < DIMENSION; a++)
		{
       cout << swarm[i][a] << '\t';
    }
     cout << '\n';
  }
   */
  for (int z = 0; z < Number; z++) velocity[z][0] = 0.01 * vel1[z];
	for (int z = 0; z < Number; z++) velocity[z][1] = 0.01 * vel2[z];
	for (int z = 0; z < Number; z++) velocity[z][2] = 0.01 * vel3[z];
	for (int z = 0; z < Number; z++) velocity[z][3] = 0.01 * vel4[z];
	for (int z = 0; z < Number; z++) velocity[z][4] = 0.01 * vel5[z];
	for (int z = 0; z < Number; z++) velocity[z][5] = 0.01 * vel6[z];
	for (int z = 0; z < Number; z++) velocity[z][6] = 0.01 * vel7[z];
	for (int z = 0; z < Number; z++) velocity[z][7] = 0.01 * vel8[z];
	for (int z = 0; z < Number; z++) velocity[z][8] = 0.01 * vel9[z];
	for (int z = 0; z < Number; z++) velocity[z][9] = 0.01 * vel10[z];
	for (int z = 0; z < Number; z++) velocity[z][10] = 0.01 * vel11[z];
	for (int z = 0; z < Number; z++) velocity[z][11] = 0.01 * vel12[z];
	for (int z = 0; z < Number; z++) velocity[z][12] = 0.01 * vel13[z];
	for (int z = 0; z < Number; z++) velocity[z][13] = 0.01 * vel14[z];
	for (int z = 0; z < Number; z++) velocity[z][14] = 0.01 * vel15[z];
	for (int z = 0; z < Number; z++) velocity[z][15] = 0.01 * vel16[z];
	for (int z = 0; z < Number; z++) velocity[z][16] = 0.01 * vel17[z];
	BestGlob[DIMENSION] = RAND_MAX;
  
  double **additional_points;
  
  ifstream best_points;
	best_points.open("./IN/best_points.txt");
	char buffer_points[256];
  int num_of_best_points = 0;
	if (best_points.is_open()){
		int points_row = 0;
    int points_column = 0;
    string points;
    best_points >> points;
    strcpy(buffer_points, points.c_str());
	  num_of_best_points = atoi(buffer_points);
    additional_points = new double *[num_of_best_points];
    for (int k = 0; k < num_of_best_points; k++){
      additional_points[k] = new double[DIMENSION];
    }
    while (best_points >> points) {
	    strcpy(buffer_points, points.c_str());
		  additional_points[points_row][points_column] = atof(buffer_points);
		  points_column++;
      if(points_column % DIMENSION == 0){
        points_row++;
        points_column = 0;
      }
		}
     for (int k = 0; k < num_of_best_points; k++){
       for (int j = 0; j < DIMENSION; j++){
         swarm[k][j] = additional_points[k][j];
       }
     }
     
   }
		best_points.close();
  
	delete[] swar1;
	delete[] swar2;
	delete[] swar3;
	delete[] swar4;
	delete[] swar5;
	delete[] swar6;
	delete[] swar7;
	delete[] swar8;
	delete[] swar9;
	delete[] swar10;
	delete[] swar11;
	delete[] swar12;
	delete[] swar13;
	delete[] swar14;
	delete[] swar15;
	delete[] swar16;
	delete[] swar17;
 
  delete[] vel1;
	delete[] vel2;
	delete[] vel3;
	delete[] vel4;
	delete[] vel5;
	delete[] vel6;
	delete[] vel7;
	delete[] vel8;
	delete[] vel9;
	delete[] vel10;
	delete[] vel11;
	delete[] vel12;
	delete[] vel13;
	delete[] vel14;
	delete[] vel15;
	delete[] vel16;
	delete[] vel17;
}

double convert(double value, double From1, double From2, double To1, double To2)
{
	return (value - From1) / (From2 - From1)*(To2 - To1) + To1;
}
void MoveWasp() //Moves the whole wasp
{
	int tmp = 1;
	int *seed = &tmp;
	double max_coord[DIMENSION];
	double min_coord[DIMENSION];
	for (int i = 0; i < Number; i++)
	{
		for (int a = 0; a < DIMENSION; a++)
		{
			velocity[i][a] = INERT * velocity[i][a] +
				(r4_uniform_01(seed)) * GLOBAL * (BestGlob[a] - swarm[i][a]);
			tmp++;
			velocity[i][a] += (r4_uniform_01(seed)) * PERSONAL * (BestPers[i][a] - swarm[i][a]);

			swarm[i][a] = swarm[i][a] + velocity[i][a];
			if (i == 0) {
				max_coord[a] = min_coord[a] = swarm[i][a];
			}
			else if (max_coord[a] < swarm[i][a]) {
				max_coord[a] = swarm[i][a];
			}
			else if (min_coord[a] > swarm[i][a]) {
				min_coord[a] = swarm[i][a];
			}
		}
		tmp++;
	}
	cout << endl;
	for (int i = 0; i < Number; i++)
	{
		for (int a = 0; a < DIMENSION; a++)
		{
			if (swarm[i][a] > UP_BOUNDS[a] || swarm[i][a] < LOW_BOUNDS[a]) {
				swarm[i][a] = convert(swarm[i][a], min_coord[a], max_coord[a], LOW_BOUNDS[a], UP_BOUNDS[a]);
			}
		}
	}
  
  /*for (int i = 0; i < Number; i++)
	{
		for (int a = 0; a < DIMENSION; a++)
		{
       cout << swarm[i][a] << '\t';
    }
     cout << '\n';
  }*/
}

void checkBP(int i, double a)
{
	if (a < BestPers[i][DIMENSION])
	{
		BestPers[i][DIMENSION] = a;
		for (int n = 0; n < DIMENSION; n++)
		{
			BestPers[i][n] = swarm[i][n];
		}
	}
}

void checkBG(int i, double a)
{
	if (a < BestGlob[DIMENSION])
	{
		BestGlob[DIMENSION] = a;
		for (int n = 0; n < DIMENSION; n++)
		{
			BestGlob[n] = swarm[i][n];
		}
	}
}

void display(int i)
{
	std::cout << i << "\t";
 std::cout << BestGlob[DIMENSION] << "\t";
	for (int n = 0; n < DIMENSION; n++)
	{
		std::cout << BestGlob[n] << "\t";
	}
	std::cout << "\n";
}

void go(int args, char* argv[])
{
	counter = 1;
  int loc_counter = 1;
  fstream f;
	f.open("./OUT/swarm.txt", ios::out);
  fstream out;
  out.open("./OUT/best_points.txt", ios::out);
  
  double step = (max_inert - INERT) / num_of_iteration;
 
  int myrank, total;
  double *A, *C;
  int n = Number; 
  int m;
  A = (double *) malloc (sizeof(double)*n*DIMENSION);
  C = (double *) malloc (sizeof(double)*n);
  double *a, *c;

  int i, j;
  int intBuf[2];

  MPI_Init (&args, &argv);
  MPI_Comm_size (MPI_COMM_WORLD, &total);
  MPI_Comm_rank (MPI_COMM_WORLD, &myrank);
  printf ("Total=%d, rank=%d\n", total, myrank);

  if (!myrank) {  
    intBuf[0] = n;
    intBuf[1] = n/total;  
    time1 = MPI_Wtime();
  }
  
  MPI_Bcast((void *)intBuf, 2, MPI_INT, 0, MPI_COMM_WORLD);
  n = intBuf[0];
  m = intBuf[1];

  a = (double *) malloc (sizeof(double)*DIMENSION*m);
  c = (double *) malloc (sizeof(double)*m);

  
	while (counter <= num_of_iteration)
	{  
  if(!myrank){
  int counter_tmp = 0;
   for (i = 0; i < Number; i++){
      for (j = 0; j < DIMENSION; j++){
	      A[counter_tmp] = swarm[i][j];
        counter_tmp++;
        }
      }
      
  }

  MPI_Scatter((void *)A, DIMENSION*m, MPI_DOUBLE,
	      (void *)a, DIMENSION*m, MPI_DOUBLE, 0, MPI_COMM_WORLD);

  for (i = 0; i < m; i++) {
    c[i] = calculate_swarm_parallel(i, a);
  }

  MPI_Gather((void *)c, m, MPI_DOUBLE,
	    (void *)C, m, MPI_DOUBLE, 0, MPI_COMM_WORLD);


   if(!myrank){

  	 for(int i = 0; i < Number; i++){
         checkBP(i, C[i]);
	       checkBG(i, C[i]);
     }
  
   if(counter < num_of_steps){
   errors[counter] = BestGlob[DIMENSION];
   }
   

		display(counter);
		if (f) {
			f << counter << '\t';
      f << BestGlob[DIMENSION] << '\t';
			for (int i = 0; i < DIMENSION; ++i)
			{
				f << fixed << setprecision(6) << BestGlob[i] << '\t';
			}
			f << '\n';
		}
   
	  if (out && (loc_counter == period)) {
		for (int i = 0; i < DIMENSION; i++)
		{
			out << fixed << setprecision(15) << BestGlob[i] << '\t';
		}
		out << '\n';
    loc_counter = 0;
    }
   if(counter >= num_of_steps){
   for(int p = 0; p < num_of_steps - 1; p++){
     errors[p] = errors[p+1];
   }
   errors[num_of_steps-1] = BestGlob[DIMENSION];
   if(errors[0] - errors[num_of_steps-1] < deflection){
   break;
   }
   }
		counter++;
    loc_counter++;
		MoveWasp();
   }
  }
  free(a);
  free(c);
  if(!myrank){
	f.close();
  out.close();
  free(A);
  free(C);
  time2 = MPI_Wtime();
  duration = time2 - time1;
  }
  
  return;
  MPI_Finalize();
}


/*           */

void init_nelder()
{
	int tmp = 123;
	int *seed = &tmp;

	for (int i = 0; i < DIMENSION + 1; i++)
	{
		for (int j = 0; j < DIMENSION; j++)
		{
			if (i == j) {
				X[j][i] = BestGlob[j] + r4_uniform_01(seed) / 10;
				X[j][DIMENSION] = BestGlob[j] - r4_uniform_01(seed) / 10;
				tmp++;

			}
			else {
				X[j][i] = BestGlob[j];
			}
		}
	}
}

void sort() {
	double MAX1 = f[0], MIN = f[0], MAX2 = f[0];
	max1 = 0;
	max2 = 0;
	min1 = 0;
	for (int i = 0; i < DIMENSION + 1; i++)
	{
		if (MAX1 < f[i])
		{
			MAX1 = f[i];
			max1 = i;
		}

		if (MIN > f[i])
		{
			MIN = f[i];
			min1 = i;
		}
	}

	for (int i = 0; i < DIMENSION + 1; i++)
	{
		if ((MAX2 < f[i]) && (i != max1))
		{
			MAX2 = f[i];
			max2 = i;
		}
	}

	for (int i = 0; i < DIMENSION; i++)
	{
		x_h[i] = X[i][max1];
		x_g[i] = X[i][max2];
		x_l[i] = X[i][min1];
	}
}

void center_mass()
{
	for (int i = 0; i < DIMENSION; i++)
	{
		double summ = 0;
		for (int j = 0; j < DIMENSION + 1; j++)
		{
			if (j != max1)
			{
				summ += X[i][j];
			}
		}
		x_c[i] = summ / DIMENSION;
	}

}

void reflection()
{
	for (int i = 0; i < DIMENSION; i++)
	{
		x_r[i] = (1 + alpha_1) *  x_c[i] - alpha_1 * x_h[i];
	}
}

void stretching()
{
	for (int i = 0; i < DIMENSION; i++)
	{
		x_e[i] = (1 - gamma_1) * x_c[i] + gamma_1 * x_r[i];
	}
}

void compression()
{
	for (int i = 0; i < DIMENSION; i++)
	{
		x_s[i] = beta * x_h[i] + (1 - beta) * x_c[i];
	}
}

void global_compression()
{
	sort();
	for (int i = 0; i < DIMENSION; i++)
	{
		for (int j = 0; j < DIMENSION + 1; j++)
		{
			if (j != min1)
			{
				X[i][j] = X[i][min1] + (X[i][j] - X[i][min1]) / 2;
			}
		}

	}
}
double variance() {
	double disper = 0;
	double middle[DIMENSION];
	for (int i = 0; i < DIMENSION; i++)
	{
		double sum = 0;
		for (int j = 0; j < DIMENSION + 1; j++)
		{
			sum += X[i][j];
		}
		middle[i] = sum / (DIMENSION + 1);
	}

	for (int i = 0; i < DIMENSION; i++)
	{
		double d = 0;
		for (int j = 0; j < DIMENSION + 1; j++)
		{
			d += pow((X[i][j] - middle[i]), 2);
		}
		disper += d;
	}
	return disper;
}

void console()
{
	fstream out;
	out.open("./OUT/nelder.txt", ios::out);
	if (out) {
		out << "num_of_iteration: " << counter_neld - 1 <<", variance: " << variance() << '\n';
   	out << calculating(x_l) << '\t';
		for (int i = 0; i < DIMENSION; ++i)
		{
			out << fixed << setprecision(15) << x_l[i] << '\t';
		}
		out << '\n';
	}
  std::cout << calculating(x_l) << "\n";
	for (int n = 0; n < DIMENSION; n++)
	{

		std::cout << x_l[n] << "\n";
	}

	
	std::cout << "\n";
	out.close();
}

void nelder() {
	for (int p = 0; p < 1; p++) {
		//step1
		init_nelder();

		for (int i = 0; i < DIMENSION + 1; i++)
		{
			f[i] = calculate(i);
		}

		while (variance() > dispersia)
		{	//step2
			if (counter_neld > max_counter_neld)
				break;
			counter_neld++;
      
      
  double max_coord[DIMENSION];
	double min_coord[DIMENSION];
	for (int i = 0; i < DIMENSION + 1; i++)
	{
		for (int a = 0; a < DIMENSION; a++)
		{
			if (i == 0) {
				max_coord[a] = min_coord[a] = X[a][i];
			}
			else if (max_coord[a] < X[a][i]) {
				max_coord[a] = X[a][i];
			}
			else if (min_coord[a] > X[a][i]) {
				min_coord[a] = X[a][i];
			}
		}
	}

	for (int i = 0; i < DIMENSION + 1; i++)
	{
		for (int a = 0; a < DIMENSION; a++)
		{
			if (X[a][i] > UP_BOUNDS[a] || X[a][i] < LOW_BOUNDS[a]) {
				X[a][i] = convert(X[a][i], min_coord[a], max_coord[a], LOW_BOUNDS[a], UP_BOUNDS[a]);
			}
		}
	}
 
      
      
			sort();
			//console();
			//step3
			center_mass();
			//step4
			reflection();
			//step5
			double f_r = calculating(x_r);
			double f_g = calculating(x_g);
			double f_h = calculating(x_h);
			double f_l = calculating(x_l);

			if (f_r < f_l)
			{
				stretching();
				double f_e = calculating(x_e);
				if (f_e < f_r)
				{	//x_h = x_e
					for (int i = 0; i < DIMENSION; i++)
					{
						X[i][max1] = x_e[i];
					}
					f[max1] = calculating(x_e);
					//step9
					continue;
				}
				else if (f_r < f_e)
				{	//x_h = x_r
					for (int i = 0; i < DIMENSION; i++)
					{
						X[i][max1] = x_r[i];
					}
					f[max1] = calculating(x_r);
					//step9
					continue;
				}
			}

			else if ((f_l < f_r) && (f_r < f_g))
			{
				//x_h = x_r
				for (int i = 0; i < DIMENSION; i++)
				{
					X[i][max1] = x_r[i];
				}
				f[max1] = calculating(x_r);
				//step9
				continue;
			}

			else if ((f_g < f_r) && (f_r < f_h))
			{
				double x_new[DIMENSION];
				for (int i = 0; i < DIMENSION; i++)
				{
					x_new[i] = X[i][max1];
					X[i][max1] = x_r[i];
					x_r[i] = x_new[i];
				}
				double f_new;
				f_new = f_h;
				f_h = f_r;
				f_r = f_new;
				f[max1] = f_r;
				//6step
				compression();
				double f_s = calculating(x_s);
				//7step
				if (f_s < f_h)
				{
					for (int i = 0; i < DIMENSION; i++)
					{
						X[i][max1] = x_s[i];
					}
					f[max1] = calculating(x_s);
					//step9
					continue;
				}
				//8step
				else if (f_s > f_h)
				{
					global_compression();
					for (int i = 0; i < DIMENSION + 1; i++)
					{
						f[i] = calculate(i);
					}
				}
				//9step
				continue;
			}

			else if (f_h < f_r)
			{
				//6step
				compression();
				double f_s = calculating(x_s);
				//7step
				if (f_s < f_h)
				{
					for (int i = 0; i < DIMENSION; i++)
					{
						X[i][max1] = x_s[i];
					}
					f[max1] = calculating(x_s);
					//9step
					continue;
				}
				//8step
				else if (f_s > f_h)
				{
					global_compression();
					for (int i = 0; i < DIMENSION + 1; i++)
					{
						f[i] = calculate(i);
					}
				}
				//9step
				continue;
			}
		}
	cout << "iteration_of_nelder: " << counter_neld - 1 << '\n';
		console();
	}


	return;
}
int main(int args, char* argv[])
{
	if (flag) {
		double variables_temp[DIMENSION];
		ifstream values;
		values.open("./IN/values.txt");
		string val_temp;
		char val_tmp[256];
		if (!values.is_open()) cout << "error";
		int counter_tmp = 0;
		while (values >> val_temp) {
			strcpy(val_tmp, val_temp.c_str());
			variables_temp[counter_tmp] = atof(val_tmp);
			counter_tmp++;
			if (counter_tmp >= DIMENSION) {
				break;
			}
		}
		double val = calculate(variables_temp);
		cout << val;
		values.close();
		cin.get();
	}
	else {
		init_swarm();
		go(args, argv);
   unsigned int start =  clock();
		nelder();
   unsigned int stop =  clock();
   
   
   fstream time;
		time.open("./OUT/time.txt", ios::out);
		if (time) {
			time << "time of parallel section: " << duration << '\n';
			time << "time of nelder-mead: " << (stop - start) / CLOCKS_PER_SEC  << '\n';
      //time << "all time: " <<global + (stop - stop_parallel) / 1000000 << '\n';
		}
		time.close();
	}
	return 0;
}
